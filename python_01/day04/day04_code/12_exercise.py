# 假设一个用户信息如下:
#  用户名是:root
#  密码是: 123456
# 写一个身份验证的程序，让用户输入用户名和密码登录, 用户名和密码全部匹配，提示登录成功。
# 否则继续，最多尝试3次。3次不匹配以后提示登录失败.

# 导入 getpass 模块
import getpass

for times in range(3):
    username = input('请输入用户名:')
    # password = input('请输入密码:')
    password = getpass.getpass('请输入密码:')       # 课间休息：15：24 回来
    if username != 'root' or password != '123456':
        print('信息错误！')
        continue
    else:
        print('登陆成功！')
        break
else:
    print('登陆失败！')

