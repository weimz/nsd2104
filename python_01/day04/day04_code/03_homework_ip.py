# 已知, 局域网的IP 都是: 192.168.0.xxx
# 写程序，生成 从 192.168.0.1 ~ 192.168.0.254 区间的 254 个 IP 地址

ip_format = '192.168.0.%d'

number = 1
while number <= 254:
    print('192.168.0.' + str(number))  # print(ip_format % number)
    number += 1
