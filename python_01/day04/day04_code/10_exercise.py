# 写程序，计算 1 + 3 + 5 + 7 + ..... + 97 + 99 的和
# 分别用 for 循环和 while 实现。

# 用 for 实现。
total = 0
for x in range(1, 100, 2):
    total += x
print('和是', total)

# 用 while 实现。
total = 0
x = 1
while x < 100:
    total += x
    x += 2
print('和是', total)

# 用 sum 函数
print('和是', sum(range(1, 100, 2)) )


