# 写一个程序， 07_input_score.py, 输入一个学生的成绩，
# 如果成绩在 [0,60)  提示"不及格"
# 如果成绩在 [60,80)  提示"及格"
# 如果成绩在 [80,90)  提示"良好"
# 如果成绩在 [90,100]  提示"优秀"
# 如果是其他值，则提示“您的输入有误”

score = float(input('请输入您的成绩: '))
if 0 <= score < 60:
    print('不及格')
elif 60 <= score < 80:
    print('及格')
elif 80 <= score < 90:
    print('良好')
elif 90 <= score <= 100:
    print('优秀')
else:
    print('您的输入有误')