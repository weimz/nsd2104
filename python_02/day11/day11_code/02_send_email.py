from email.mime.text import MIMEText
from email.header import Header
import smtplib
import getpass

def inet_email(
        body, # 邮件的内容，
        sender,  # 发送邮件的人的邮箱
        receivers,  # 接收邮件的email地址的列表
        subject,   # 邮件的主题
        host,   # 邮件服务器的主机地址
        passwd  # 邮箱的密码
):
    # 准备正文，plain表示纯文本内容
    message = MIMEText(body, 'plain', 'utf8')
    # 设置邮件的头部消息
    message['From'] = Header(sender, 'utf8')
    message['To'] = Header(receivers[0], 'utf8')
    message['Subject'] = Header(subject, 'utf8')

    # 发送邮件
    smtp = smtplib.SMTP()
    smtp.connect(host)
    smtp.login(sender, passwd)
    smtp.sendmail(sender, receivers, message.as_bytes())

if __name__ == '__main__':
    body = 'NSD2104 Python 电子邮件测试\n 发送者: 魏老师'
    sender = '77878045@qq.com'
    receivers = ['77878045@qq.com', 'weimz@tedu.cn']
    subject = 'my python email test'
    host = 'smtp.qq.com'  # QQ 邮箱服务器
    # passwd = getpass.getpass('请输入授权码：')
    passwd = 'bsodterhkqgucbbb'
    inet_email(body, sender, receivers, subject, host, passwd)