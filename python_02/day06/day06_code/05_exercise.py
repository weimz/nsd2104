# 写一个函数 myadd, 此函数传入两个参数， x, y
# 次函数的功能是打印  x + y 的和:
# def myadd(......):
#      ....
#
# myadd(100, 200)   # 300
# myadd('ABC', '123')  # ABC123


def myadd(x, y):
    print(x + y)

myadd(100, 200)   # 300
myadd('ABC', '123')  # ABC123