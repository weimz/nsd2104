

class Car:
   pass

car1 = Car()  # 创建第一辆汽车(红色 比亚迪)
car2 = Car()  # 创建第二辆汽车(白色 宾利)
car1.color = '红色'    # 颜色
car1.brand = '比亚迪'  # 品牌 
car2.color = '白色'    # 颜色
car2.brand = '宾利'    # 品牌 

print(car2.color, '的', car2.brand, '正在飞驰')
print(car1.color, '的', car1.brand, '正在停车')

