
signals = ['石头', '剪刀', '布']
try:
    x = int(input("请输入一个整数："))  # int('一百')
    print(signals[x])   # 可能会产生 IndexError 类型的错误
    print('hello world!')

except ValueError as err:
    x = 0
else:
    print('try 语句内部没有发生错误！')
finally:
    print('我是finally 子句，我无论如何都会执行')

print('x=', x)
print('程序正常结束')