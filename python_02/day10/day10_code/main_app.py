
from narcissistic import print_narcissistic
from table99 import print_99_table

while True:    # 课间休息: 10:12 回来
    print('1) 打印水仙花数')
    print('2) 打印99乘法表')
    print('0) 退出')
    x = input('请选择:')
    if x == '1':
        print_narcissistic()
    elif x == '2':
        print_99_table()
    elif x == '0':
        break