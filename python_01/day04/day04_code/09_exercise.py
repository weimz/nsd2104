# 用 for 语句 打印 1~20 的整数，打印在一行内
# 1 2 3 4 .... 19 20

for x in range(1, 21):
    print(x, end=' ')

print()  # 换行
