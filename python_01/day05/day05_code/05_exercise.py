# 写程序，输入任意行文字。当没有输入的时候时结束，
# 将所有输入的字符串先保存在一个列表L1中 ,
# 将L1 中的字符串的长度都计算出来，存入列表L2 中
# 1） 然后打印最长的字符串的字符个数
# 2） 打印最长的那个字符串
# 如:
#    请输入: hello
#    请输入: abcdef
#    请输入: a
#    请输入: <直接回车结束输入>
# 打印：
#    最长的字符个数是: 5
#    最长的字符串是: abcdef
# L1 = ['hello', 'abcdef', 'a']
# L2 = [5, 6, 1]
# mymax = 6    # 求出最长的字符串的长度
# pos = L2.index(6)  # 求出最长的字符串的位置
# ...

L1 = []
while True:
    s = input('请输入：')
    if s == '':
        break
    L1.append(s)
# 计算 L1 中字符串的长度放在 L2 中

L2 = [len(s) for s in L1]
# L2 = []
# for s in L1:
#     L2.append(len(s))

print('L1=', L1)
print('L2=', L2)
mymax = max(L2)  # 求最长的字符串的长度
pos = L2.index(mymax)   # 求最长的字符串的索引
print('L2中最大数的索引是：', pos)
print('您输入的最长的字符串是:', L1[pos])