class Human:  # 定义一个人类
    def say(self, what):
        print("说:", what)
  
    def walk(self, distance):
        print('走了', distance, '公里')
  
class Student(Human):
    def study(self, subject):
        print('学习', subject)

class Teacher(Human):
    def teach(self, subject):
        print('教', subject)

h1 = Human()
h1.say('天气真好')
h1.walk(5)

s1 = Student()
s1.walk(4)
s1.say('有点累')
s1.study('面向对象')

t1 = Teacher()
t1.teach('Python')
t1.say('今天周五')
t1.walk(6)

