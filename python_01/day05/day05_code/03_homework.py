# 写程序，输入一个整数，打印如下的正方形
# 如：
# 请输入: 4
# 打印:
# 1 2 3 4
# 2 3 4 5
# 3 4 5 6
# 4 5 6 7
# 请输入: 5
# 打印:
# 1 2 3 4 5
# 2 3 4 5 6
# 3 4 5 6 7
# 4 5 6 7 8
# 5 6 7 8 9

n = int(input('请输入: '))

for x in range(1, n+1):
    # print(x)  # <===== 替换他
    for y in range(x, x + n):
        print(y, end=' ')
    print()  # 换行



