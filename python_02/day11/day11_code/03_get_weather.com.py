
# 使用 request 模块向 中国天气网的网站
# 发送GET 请求，来获取北京，上海等天气的信息
# 北京的URL地址：
# http://www.weather.com.cn/data/sk/101010100.html
# 上海的URL地址：
# http://www.weather.com.cn/data/sk/101020100.html

#1. 导入 requests 模块
import requests
import json

# 2. 发送GET请求
url = 'http://www.weather.com.cn/data/sk/101010100.html'
resp = requests.get(url)
# 3. 使用 resp.content 获取网站返回的字节串
# print(resp.content)
# 4. 使用字节串的 decode() 方法，将 resp.content 字节从转换为
# 字符串
s_json = resp.content.decode()
# 5. 将 json 转化为 Python 的数据类型
# print(s_json)
obj = json.loads(s_json)  # 课间休息： 15：13 回来
print(obj)
print('城市：', obj["weatherinfo"]['city'])
print('风速：', obj["weatherinfo"]['WS'])