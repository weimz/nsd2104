## python_01 day03笔记

- day02 回顾

    - 表达式

        - 字符串

            ```
            'hello'
            ```

        - 数字

            ```
            100
            ```

        - 运算符

            - 算术运算符  `+ - * / // % ** `

            - 比较运算符

                ```
                <  <=   >   >=  ==   !=
                ```

            - 布尔运算

                ```
                and     or    not
                ```

            - 条件表达式

                ```python
                左表达式 if 条件表达式 else 右表达式
                ```

        - 函数

            ```
            print(...)
            input(...)
            int(...)
            float(...)
            str(...)
            bool(...)
            ```

    - 语句

        - 赋值语句

            ```python
            a = 100
            ```

        - 表达式语句

            > 表达式单独放在一行形成的语句叫做表达式语句

            ```python
            print(a)
            ```

        - del 语句

            ```python
            del a
            ```

        - pass 语句

            ```
            if True:
                pass
            ```

        - if 语句（复合语句）

            ```python
            if  条件表达式1:
                语句块1
            elif 条件表达式2:
                语句块2
            elif 条件表达式3:
                语句块3
            elif 条件表达式n:
                语句块n
            else:
                语句块other
            ```

- 运算符的优先级

```
1 + 3 * 3 ** 2  + 4 // 2
1 + 3 * 9 + 4 // 2
1 + 27 + 4 // 2
1 + 27 + 2
28 + 2
30
```

- 运算符优先级表

| 运算符                                                       | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| `(expressions...)`,`[expressions...]`, `{key: value...}`, `{expressions...}` | 绑定或加圆括号的表达式，列表显示，字典显示，集合显示         |
| `x[index]`, `x[index:index]`, `x(arguments...)`, `x.attribute` | 抽取，切片，调用，属性引用                                   |
| [`await`](https://docs.python.org/zh-cn/3/reference/expressions.html#await) `x` | await 表达式                                                 |
| `**`                                                         | 乘方 [5](https://docs.python.org/zh-cn/3/reference/expressions.html#id21) |
| `+x`, `-x`, `~x`                                             | 正，负，按位非 NOT                                           |
| `*`, `@`, `/`, `//`, `%`                                     | 乘，矩阵乘，除，整除，取余 [6](https://docs.python.org/zh-cn/3/reference/expressions.html#id22) |
| `+`, `-`                                                     | 加和减                                                       |
| `<<`, `>>`                                                   | 移位                                                         |
| `&`                                                          | 按位与 AND                                                   |
| `^`                                                          | 按位异或 XOR                                                 |
| `|`                                                          | 按位或 OR                                                    |
| [`in`](https://docs.python.org/zh-cn/3/reference/expressions.html#in), [`not in`](https://docs.python.org/zh-cn/3/reference/expressions.html#not-in), [`is`](https://docs.python.org/zh-cn/3/reference/expressions.html#is), [`is not`](https://docs.python.org/zh-cn/3/reference/expressions.html#is-not), `<`, `<=`, `>`, `>=`, `!=`, `==` | 比较运算，包括成员检测和标识号检测                           |
| [`not`](https://docs.python.org/zh-cn/3/reference/expressions.html#not) `x` | 布尔逻辑非 NOT                                               |
| [`and`](https://docs.python.org/zh-cn/3/reference/expressions.html#and) | 布尔逻辑与 AND                                               |
| [`or`](https://docs.python.org/zh-cn/3/reference/expressions.html#or) | 布尔逻辑或 OR                                                |
| [`if`](https://docs.python.org/zh-cn/3/reference/expressions.html#if-expr) -- `else` | 条件表达式                                                   |
| [`lambda`](https://docs.python.org/zh-cn/3/reference/expressions.html#lambda) | lambda 表达式                                                |
| `:=`                                                         | 赋值表达式                                                   |



### 字符串

- 字面值(前期知识回顾)

  ```python
  'hello'    "hello"     '''hello'''    """hello"""
  ```

- 字符串转义

  ```
  \'     代表一个单个的 '
  \n
  ```

- 字符串的运算

  \+ 用于拼接字符串

  \*  用于生成重复的字符串

- 原始字符串(raw 字符串)

  > 以字符r开头的字符串, 其中的 \ 不算转义字符串

  字面值格式

  ```
  r'字符串内容'
  r"字符串内容"
  r'''字符串内容'''
  r"""字符串内容"""
  ```

- in / not in 运算符

  - 作用

    in 用于序列，字符串，字典中，用于判断某个值是否存在于容器中,如果存在就返回True, 否则返回False

    not in 的返回值与 in 相反

  - 示例

    ```python
    >>> s = 'welcome to beijing!'
    >>> 'to' in s
    True
    >>> 'weimingze' in s
    False
    >>> 'beijing' not in s
    False
    >>> 'weimz' not in s
    True
    ```

- 字符串的索引 index

  - 语法

    ```
    字符串[整数表达式]
    ```

  - 作用

    用来获取字符串其中的一个字符

  - 说明

    正向索引: 字符串的 正向索引是从0开始的，.... 最后一个索引为 len(s)-1

    反向索引: 字符串的反向索引是从 -1 开始的，-1 代表最后一个，... -len(s) 代表第一个

  - 示例

    ```
    >>> s = 'ABCDE'
    >>> s[1]
    B
    >>> s[1+2]
    D
    >>> s[-1]
    E
    ```

    > 注: len(x) 函数可以求取一个字符串的长度

- 练习

  ```
  写程序，输入一个字符， 打印字符串的如下内容
  1. 打印这个字符串的第一个字符
  2. 打印这个字符串的最后一个字符串
  3. 如果这个字符串的长度是 奇数，则打印中间这个字符
     用 len(x) 求字符串的长度
  ```

- 字符串的切片 slice

  - 作用

    从字符串序列中取出相应位置的元素，重新组成一个新的字符串

  - 语法

    ```
    字符串[(开始索引b):(结束索引e)(:(步长s))]
    ```

    > 注: () 括起来的内容代表可以省略

  - 说明

    - 开始索引b是切片切下的开始位置

    - 结束索引e 是切片的终止位置(但不包含终止点)
    - 步长是切片取出数据后向后移动的方向和偏移量
      - 没有步长，相当于步长为1(默认为1)
      - 当步长为正数时，取正向切片
        - 开始索引默认为0, 结束索引是最后一个元素的下一个索引
      - 当步长为负数时，取反向切片
        - 开始索引默认为-1, 结束索引是第一个元素的前一个索引

  - 示例

    ```
         #   01234
    >>> s = 'ABCDE'
    >>> s[1:]    # 'BCDE'
    >>> s[-2:]   # 'DE'
    >>> s[1:4]   # 'BCD'
    >>> s[:]     # 'ABCDE'
    >>> s[:2]    # 'AB'
    >>> s[1:1]   # ''
    >>> s[4:2]   # ''
    >>> s[::]    # 等同于 s[::1] 'ABCDE'
    >>> s[::2]   # 'ACE'
    >>> s[2::2]  # 'CE'
    >>> s[-1::-2] # 'ECA'
    >>> s[::-1]   # 'EDCBA'
    ```

- 练习1

  ```
  写一个程序，输入一个字符串，把字符串的第一个字符和最后一个字符去掉，打印处理后的字符串
  ```

- 练习2

  ```
  输入一个字符串, 判断这个字符串是否是回文
  上海自来水来自海上
  ABCCBA
  13531
  ```

### 字符串的格式化表达式

- 作用

  生成具有一定格式的字符串

- 语法规则

  ```
  格式化字符串 % 参数1
  # 或者
  格式化字符串 % (参数1， 参数2, 参数3)
  ```

- 占位符和类型码


| 占位符和类型码 | 说明                                   |
| :------------: | -------------------------------------- |
|       %s       | 转成字符串, 使用 str(x) 函数转换(常用) |
|       %d       | 转成 十进制的整数(常用)                |
|       %o       | 转成 八进制的整数                      |
|     %x,%X      | 转成 十六进制的整数                    |
|     %e,%E      | 转成 指数格式的浮点数                  |
|     %f,%F      | 转成小数格式的浮点数(常用)             |
|     %g,%G      | 转成指数格式或小数格式的浮点数         |
|       %%       | 转成一个%                              |




- 示例1

```
>>> fmt = """
甲方:_____%s_____
乙方:_____%s_____
   ....... 金额:_%d__
"""
>>> s = fmt % ("魏眀择", '达内', 10000)
>>> print(s)

甲方:_____魏眀择_____
乙方:_____达内_____
   ....... 金额:_10000__

```

- 示例2

```
>>> "+++%d+++" % 999    # 返回 '+++999+++'
'+++999+++'
>>> "+++%s+++" % 3.14
'+++3.14+++'
>>> "+++%s+++" % "weimz"
'+++weimz+++'
>>> "+++%d+++" % 3.1415
'+++3+++'
>>> "+++%d+++" % 13
'+++13+++'
>>> "+++%o+++" % 13
'+++15+++'
>>> "+++%x+++" % 13
'+++d+++'
>>> "+++%e+++" % 3.1415
'+++3.141500e+00+++'
>>> "+++%f+++" % 3.1415
'+++3.141500+++'
>>> "+++%g+++" % 3.1415
'+++3.1415+++'
>>> "+++%g+++" % 3141523274812374281342374324.23748123742183472
'+++3.14152e+27+++'

```

- 占位符和类型码之间的格式化语法

  ```
  % [- + 0 宽度.精度] 类型码
  ```

  ```
  - 左对齐
  + 显示正号
  0 左侧空白位置补零
  宽度： 整个数据输出的宽度
  精度: 保留小数点以后多少位(默认小数点以后保留6位)
  常用于 %d 和 %f 中
  ```

  - 示例

  ```python
  >>> "--%d--" % 99
  '--99--'
  >>> "--%5d--" % 99
  '--   99--'
  >>> "--%05d--" % 99
  '--00099--'
  >>> "--%5d--" % 99
  '--   99--'
  >>> "--%+5d--" % 99
  '--  +99--'
  >>> "--%-5d--"  % 99
  '--99   --'
  >>> "--%f--" % 3.1415926
  '--3.141593--'
  >>> "--%.2f--" % 3.1415926
  '--3.14--'
  >>> "--%7.2f--" % 3.1415926
  '--   3.14--'
  ```

-  练习

  ```
  写一个程序:
  	输入一个字符串
  	输入一个浮点数
  	输入一个整数
  	输出: 全为10个字节宽度的字符串如下：
  如:
     请输入字符串: weimz
     请输入浮点数: 3.14
     请输入整数:   99
  打印如下:
     +------------+
     | weimz      | 
     |       3.14 |
     | 0000000099 |
     +------------+
  
  ```

  > 答案见: 05_exercise_format.py

### 字符串的常用方法  (method)

- 方法的调用语法

  ```pythoono
  对象.方法名(参数)
  ```

- 示例:

  ```pythono
  >>> "abc".isalpha() 
  True
  >>> "123".isalpha()
  False
  >>> "123".isdigit()
  True
  ```

  > . 是"的"的意思
  - 字符串的常用方法

  | 方法名（S代表字符串对象）             | 说明                                                |
  | ------------------------------------- | --------------------------------------------------- |
  | S.isalpha()                           | 判断字符串是否是英文字符                            |
  | S.isdigit()                           | 判断字符串是否是全为数字                            |
  | S.isupper()                           | 判断字符串是否是全为大写的英文字符                  |
  | S.islower()                           | 判断字符串是否是全为小写的英文字符                  |
  | S.center(width[, fill])               | 将字符串按width 宽度居中，左右默认填充空格          |
  | S.count(sub[, start[, end]])          | 获取字符串中的子串的个数                            |
  | S.startswith(prefix)                  | 返回S是否以prefix 开头                              |
  | S.endswith(suffic)                    | 返回S是否以suffic结尾                               |
  | str.replace(*old*, *new*[, *count*])  | 替换字符串                                          |
  | str.find(*sub*[, *start*[, *end*]])   | 查找字符串,返回第一次出现的索引，如果没找到，返回-1 |
  | str.index(*sub*[, *start*[, *end*]])  | 查找字符串,返回第一次出现的索引,如果没找到则报错    |
  | str.rindex(*sub*[, *start*[, *end*]]) |                                                     |

  > [] 代表其中的内容弄可以省略
  >
  > 详见： https://docs.python.org/zh-cn/3/library/stdtypes.html#string-methods

- 示例:

  ```
  >>> s = "ABC"
  >>> s.center(10)
  '   ABC    '
  >>> s.center(10, '#')
  '###ABC####'
  >>> s = 'hello world, hello china'
  >>> s.count('hello')
  2
  >>> s.count('hello', 10)
  1
  >>> s.count('hello', 10, 12)
  0
  >>> s = "/root/music/冰雨.mp3"
  >>> s.startswith('/root/')
  True
  >>> s.endswith('.mp3')
  True
  >>> s.endswith('music')
  False
  >>> s = 'welcome to Beijing'
  >>> s.upper()
  'WELCOME TO BEIJING'
  >>> s.lower()
  'welcome to beijing'
  >>> s.title()
  'Welcome To Beijing'
  ```

  ```python
  >>> s = '/root/music/冰雨.mp3'
  >>> pos = s.rindex('/')
  >>> pos
  11
  >>> pathname= s[:pos]
  >>> filename = s[pos+1:]
  >>> print(pathname)
  /root/music
  ```

  - 练习

      ```
      写一个程序，输入一个路径
      1) 打印出这个路径是否是在 /root 文件夹下
      2) 判断您的文件是否是  .sh 文件
      3) 如果是.sh 文件，如 xxx.sh, 则打印文件名 xxx
      如:
         请输入: /root/bin/abcd.sh
         1. 在 /root 下
         2. 是.sh 文件
         3. 文件名是: xxx
         
         请输入: /etc/ABCD.SH
         1. 不在 /root 下
         2. 是 .sh 文件
         3. 文件名是: ABCD
      
      ```

      

- 语句回顾

  1. 表达式语句
  2. 赋值语句   `name = input("xxxx: ")`
  3. del 语句
  4. pass 语句
  5. if 语句

- 循环语句

  - 种类

    - while 语句
    - for 语句

  - 问题:

    ```
    # 写一个程序，输入一个整数n,  打印 n 行 hello world
    print("hello world!")
    ```



### while 语句

- 作用

  根据一定的条件，重复的执行某个语句块

- 语法

  ```python
  while 真值表达式:
      语句块1  (*此部分可能会重复执行)
  else:
      语句块2
  ```

- 说明

  - else 子句可以省略
  - else 子句 当且仅当 真值表达式为假Flase的时候 会执行 else 里的语句块2
  - 如果 此 while 是因为 调用 break 语句而终止循环。则 else 子句里的语句不会执行

- 示例1

  ```python
  while 1 < 2:
      print("hello world")
  ```

- 示例2

  ```python
  # 打印 5 行 "hello world"
  i = 0
  while i < 5:
      print("hello world")
      i = i + 1  # 也可写成 i += 1
  else:
      print("while 语句已经结束!")
  ```

- 练习1

  ```
  写一个程序,输入一个整数，写程序打印如下 n 行文字
  如:
     请输入: 10
     这是第 1 行
     这是第 2 行
     这是第 3 行
     ...
     这是第 10 行
  ```

- 练习2

  ```
  写一个程序 打印1~20 的整数, 打印在 1 行内 每个数字之间用一个空格分隔开
  1 2 3 4 5 ... 19 20
  提示: print(x, end=' ')
  ```

### 语句嵌套

- while 语句也是语句, 和其他的语句一样，可以嵌套到其他的任何复合语句中。

- while  语句嵌套示意:

  ```
  while 真值表达式:
      ...
      while 真值表达式2:
          ...
      else:
          ...
      ......
  else:
      ...
  ```

  示例

  ```python
  写一个程序 打印1~20 的整数, 打印在 1 行内 每个数字之间用一个空格分隔开
  1 2 3 4 5 ... 19 20
  
  打印 10 行
  1 2 3 4 5 ... 19 20
  1 2 3 4 5 ... 19 20
  1 2 3 4 5 ... 19 20
  1 2 3 4 5 ... 19 20
  1 2 3 4 5 ... 19 20
  ....
  1 2 3 4 5 ... 19 20
  
  ```

- PyCharm 的调试

    1. 设置断点
    2. 单步执行

- 练习:

  ```
  写一个程序 打印1~20 的整数, 打印在 1 行内 每个数字之间用一个空格分隔开
  1 2 3 4 5 ... 19 20
  
  以上输入打印 10 行
  ```

- 课后练习1

  ```
  写程序，输入一个整数n , 代表结束的整数， 计算
          1 + 2 + 3 + 4 + ..... + n 的和
  请输入: 100
  5050
  # 提示： 
  he = 0  # 用一个变量来记录这个和
  ```

- 课后练习2

  ```
  打印 1~ 20 的整数, 每行打印 5 个， 打印 四行, 如:
  1 2 3 4 5
  6 7 8 9 10
  ...
  16 17 18 19 20  
  # 提示: print()  换行
  ```

- 课后练习3

  ```
  已知, 局域网的IP 都是: 192.168.0.xxx
  写程序，生成 从 192.168.0.1 ~ 192.168.0.254 区间的 254 个 IP 地址
  ```

- 课后练习4

  ```
  写一个程序，输入三行文字， 按最长的一行， 打印如下图形方框
  请输入: hello
  请输入: welcome to beijing 
  请输入: aaaaaaa
  打印:
  +--------------------+
  |       hello        |
  | welcome to beijing |
  |       aaaaaaa      |
  +--------------------+
  提示: str.center(width)
  ```
  
  答案 

  ```python
  # 写一个程序，输入三行文字， 按最长的一行， 打印如下图形方框
  # 请输入: hello
  # 请输入: welcome to beijing
  # 请输入: aaaaaaa
  # 打印:
  # +--------------------+
  # |       hello        |
  # | welcome to beijing |
  # |       aaaaaaa      |
  # +--------------------+
  # 提示: str.center(width)
  
  s1 = input("请输入：")   # ctrl + d 可以复制当前行
  s2 = input("请输入：")
  s3 = input("请输入：")
  
  len1 = len(s1)  # 求取字符串的长度
  len2 = len(s2)
  len3 = len(s3)
  # 从上面三个数中，找出最大的一个， 用 mymax 变量绑定
  # 方法2， 通用的算法
  mymax = len1
  
  if len2 > mymax:
      mymax = len2
  
  if len3 > mymax:
      mymax = len3
  
  # 方法1
  # if len1 > len2:
  #     # 让 len1 和 len3 再比较
  #     if len1 > len3:
  #         mymax = len1
  #     else:
  #         mymax = len3
  # else:
  #     # 让 len2 和 len3 再比较
  #     if len2 > len3:
  #         mymax = len2
  #     else:
  #         mymax = len3
  print('最大长度是：', mymax)
  
  # 计算第一行
  line1 = '+' + '-' * (mymax +2) + '+'
  print(line1)
  # 文字的第一行
  # 方法1 ， 用 str.center 居中文字的第一行
  # center_s1 = '| ' + s1.center(mymax) + ' |'
  
  # 方法2 ， 用 自己的方法 居中文字的第一行
  left_blanks = (mymax - len1) // 2   # 计算左侧的空格数
  right_blanks = mymax - len1 - left_blanks   # 计算右侧的空格数
  center_s1 = '| ' + ' ' * left_blanks + s1 + ' ' * right_blanks + ' |'
  print(center_s1)
  
  # 文字的第二行
  center_s2= '| ' + s2.center(mymax) + ' |'
  print(center_s2)
  # 文字的第三行
  center_s3 = '| ' + s3.center(mymax) + ' |'
  print(center_s3)
  
  # 打印最后一行
  print(line1)
  
  ```
  
  
  
  
  
  







