# 写一个程序: 输入一个整数，代表树干的高度，打印如下圣诞树
# 请输入: 3
# 打印:
#   *
#  ***
# *****
#   $
#   $
#   $
# 请输入: 4
# 打印:
#    *
#   ***
#  *****
# *******
#    $
#    $
#    $
#    $

n = int(input('请输入: '))  # 3

def print_tree(n):
    # 1. 打印树叶部分
    for line in range(1, n+1):  # line 代表行号
        # print('行：', line)
        # 计算每一行的星号的个数s
        stars = 2 * line - 1  # 代表星号的个数
        # 计算每一行空格的个数
        blanks = n - line
        aline = ' ' * blanks + '*' * stars
        print(aline)
    # 2. 打印树干部分
    for _ in range(n):
        aline = ' ' * (n-1) + '$'
        print(aline)

print_tree(n)  # 打印 树干高度为 n 的树
# print_tree(5)  # 打印 树干高度为 5 的树
