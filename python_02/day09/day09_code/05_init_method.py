

class Car:
    def __init__(self, c='透明', b='无品牌'):
        print("__init__, 被调用")
        self.color = c  # 颜色
        self.brand = b  # 品牌 

    def run(self, speed):
        print(self.color, '的', self.brand, '正在以',
            speed, 'km/h的速度行驶')

car1 = Car('红色', '比亚迪')  # 创建第一辆汽车(红色 比亚迪)
car1.run(160)

car2 = Car('白色', '宾利')  # 创建第二辆汽车(白色 宾利)
car2.run(330)

car3 = Car()
car3.run(40)




