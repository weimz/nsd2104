# 写程序，输入一个整数, 打印如下的四个三角形
# 如:
#     请输入: 3
# 1. 打印
#     *
#     **
#     ***
# 2. 打印
#       *
#      **
#     ***
# 3. 打印
#     ***
#     **
#     *
# 4. 打印
#     ***
#      **
#       *




# 写程序，输入一个整数, 打印如下的四个三角形
n = int(input('请输入：'))

print('----- (一） -------')
for stars in range(1, n + 1):
    print('*' * stars)    # stars 代表星号的个数
# 1. 打印
#     *
#     **
#     ***
print('----- (二） -------')
for stars in range(1, n+1):
    # 计算空格的个数
    blanks = n - stars
    print(' ' * blanks + '*' * stars)
# 2. 打印
#       *
#      **
#     ***

print('----- (三） -------')
for stars in range(n, 0, -1):
    print('*' * stars)
# 3. 打印
#     ***
#     **
#     *

print('----- (四） -------')
for stars in range(n, 0, -1):
    blanks = n - stars
    print(' ' * blanks + '*' * stars)
# 4. 打印
#     ***
#      **
#       *