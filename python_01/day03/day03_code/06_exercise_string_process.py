# 写一个程序，输入一个路径
# 1) 打印出这个路径是否是在 / root 文件夹下
# 2) 判断您的文件是否是.sh 文件
# 3) 如果是.sh 文件，如 xxx.sh, 则打印文件名 xxx
# 如:
#   请输入: /root/bin/abcd.sh
#     1. 在 / root 下
#     2. 是.sh 文件
#     3. 文件名是: xxx
#   请输入: / etc / ABCD.SH
#     1. 不在 / root 下
#     2. 是.sh 文件
#     3. 文件名是: ABCD


s = input('请输入一个路径：')  # /root/bin/abcd.sh
# 判断是否以 ‘/root’开头
if s.startswith('/root'):
    print('在 /root下')
else:
    print('不在/root下')

# 判断是否以 '.sh'结尾
if s.endswith('.sh') or s.endswith('.SH'):     # 课间休息： 16：27 回来
    print('是.sh 文件')
    # 找文件名
    pos = s.rindex('/')  # 找到 / 的位置
    print('文件名是：', s[pos+1:-3])
else:
   print('不是.sh 文件')

