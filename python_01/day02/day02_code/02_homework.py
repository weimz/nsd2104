# 已知从凌晨 0:0:0 到现在已经过了 63320 秒,
# 请问现在是 几时几分几秒?

s = 63320

hour = s // 60 // 60  # 计算小时
minute = s // 60 % 60  # 计算分钟
second = s % 60  # 计算秒数
print(hour, ":", minute, ":", second, sep='')
