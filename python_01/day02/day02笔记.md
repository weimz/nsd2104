## python_01 day02笔记

- day01 回顾

    - 数据类型

        - 字符串 str

            ```
            'hello'    "hello"   '''hello'''  """hello"""
            ```

            转义字符

            ```
            \n(换行new line)  \'   \"    \\      \t（制表符)   \r(回车)
            ```

            运算

            ```python
            'hello' + ' ' + 'world'      # 'hello world'
            'hello' * 3                  # 'hellohellohello'
            ```

        - 数字

            - 整数 int(字面值)

                ```python
                # 十进制
                100      0       -1          3
                # 二进制
                0b1001                       0b11
                # 八进制
                0o12345670                   0o3
                # 十六进制
                0xA1B2C3                     0x3
                0Xa1B2C3                     0X3
                ```

            - 浮点数 float

                ```python
                3.14
                0.314E1
                ```

            - 布尔类型数 bool

                ```
                True      False
                ```

    - 基本输入输出函数

        - input 函数

            ```
            name = input('请输入姓名:')
            age = input('请输入年龄:')
            ```

        - print 函数

            ```python
            print(name, age)
            ```

    - 运算符

        - 算术运算符

            ```python
            +    -    *     /       //      %        **
            ```

    - 语句

        - 赋值语句

            ```
            变量 = 表达式
            变量1 = 变量2 = 变量3 = 表达式
            变量1, 变量2 = 表达式1, 表达式2
            ```

### PyCharm的使用 

- PyCharm 下快捷键

    ctrl + s 保存

    ctrl + / 注释和取消注释

    ctrl + d 可以复制当前行到下一行

    ctrl + alt + L  可以将程序自动格式化成 符合PEP8 编码规范的格式

- Windows 安装PyCharm  的教程

    http://tedu.weimingze.com/static/python/pycharm_install.html

### del 语句

- 作用:

  用于删除变量，同时解除与对象的关联，如果可能则释放此变量绑定的对象

- 语法:  

  ```python
  del 变量名
  del 变量名1, 变量名2, ...
  ```

- 自动化内存管理和引用计数: 
  每个对象都会记录有几个变量引用自身，当引用的数量为0时则此对象被销毁，此种自动化内存管理的方式叫做引用计数。

### 比较运算符

> 比较运算符是二元运算符

- 运算符

  ```
  <    小于
  <=   小于等于
  >    大于
  >=   大于等于
  ==   等于
  !=   不等于
  ```

  > 比较运算符通常返回布尔类型的数, True, False

  - 示例

  ```python
  >>> 100 + 200 > 3
  True
  >>> 1 + 3 >= 2
  True
  >>> 1 + 3 >= 200  # 表达式
  False
  >>> score = 83    # 这是赋值语句
  >>> 60 <= score <= 100
  True
  >>> score = 59
  >>> 60 <= score <= 100
  False
  ```

### 表达式和语句的概念

- 表达式

  是由数字，字符串(文字), 运算符，函数调用等组成，通常用于计算并得到一个结果

  > 表达式是语文中的字或短语

- 语句

  语句是计算机执行程序的最小单位

  - 示例

  ```python
  a = 100      # 赋值语句
  print(a)     # 表达式语句
  ```

- 函数调用是表达式

  > 学过的函数

  > input('xxxx')  返回字符串
  >
  > print("xxxx")   返回 None

  函数调用语法规则

  ```python
  函数名(传入的参数)
  ```

  > None 是表示空值的一个对象

- 数据类型转换相关的函数

  | 函数     | 说明                          |
  | -------- | ----------------------------- |
  | str(x)   | 把传入的x 转化成字符串并返回  |
  | int(x)   | 把 x 转化为整数并返回         |
  | float(x) | 把 x 转化为浮点数并返回       |
  | bool(x)  | 把 x 转化为布尔类型的数并返回 |

  - 示例:

    ```
    >>> age = input('请输入年龄: ')  # 输入 35
    >>> int(age)
    35
    >>> int("35")
    35
    >>> int(3.14)
    3
    >>> int('3.14')  # 报错
    >>> f = 3.14
    >>> str(f)
    '3.14'
    >>> int(f)
    3
    >>> bool(f)
    True
    
    ```

  - python 中假值对象

    ```
    None
    False
    0
    0.0
    ''
    []   # 空列表
    {}   # 空字典
    ...
    ```

- 练习

  ```
  写程序，输入您的年龄, 打印出去年你几岁，明年你几岁
  用 str(x) , int(x), float(x), bool(x)
  ```

  

### 布尔运算符(也叫逻辑运算符)

- 运算符

  ```python
  and    与运算
  or     或运算
  not    非运算
  ```

- and 与运算

  两者（两个元素同时为真，结果才为真）

  - 语法

  ```python
  x and y   # x, y代表表达式
  ```

  - 示例

  ```python
  >>> 3 + 4 > 5 and 6 + 7 > 100
  ```

  - 真值表

  | x的值 | y的值 | x and y的值 |
  | :---: | :---: | :---------: |
  | True  | True  |    True     |
  | True  | False |    False    |
  | False | True  |    False    |
  | False | False |    False    |

  > 优先返回假值对象, 如果x 为假值，返回x, 否则返回y

- or  或运算

  两者（两个元素只要有一个为真，结果就为真）

  ```python
  x or y   # x, y代表表达式
  ```

- 真值表

  | x的值 | y的值 | x or y的值 |
  | :---: | :---: | :--------: |
  | True  | True  |    True    |
  | True  | False |    True    |
  | False | True  |    True    |
  | False | False |   False    |


  > 优先返回真值对象, 如果x 为真值，返回x, 否则返回y

- not 非运算

    > not 运算符是一元运算符

    语法

    ```python
    not 表达式
    ```

    示例

    ```python
    not True    # False
    not False   # True
    not 3.14    # False
    not ''      # True
    not 1 + 2   # False
    ```

    

- and 示例

  ```python
  >>> True and True    # True
  >>> True and False   # False
  >>> False and True   # False
  >>> False and False  # False
  >>> True or True    # True
  >>> True or False   # Ture
  >>> False or True   # Ture
  >>> False or False  # False
  >>> not False       # True
  >>> not True        # Flase
  >>> 3.14 and 5      # 5
  >>> 0.0 and 5       # 0.0
  >>> 3.14 or 5       # 3.14
  >>> 0.0 or 0        # 0
  >>> not 3.14        # False
  >>> not 0.0         # True
  ```

- 问题

  ```
  写一个程序,输入一个整数, 判断这个整数是奇数还是偶数
  奇数: 1 3 5 7 9
  偶数: 2 4 6 8 10    
  x % 2 == 1   结果为Ture,x 一定是奇数
  x % 2 == 1   结果为False,x 一定是偶数
  ```

### if 语句

- 作用

  让程序根据条件选择性的执行其中的某一个语句块

- 语法

  ```python
  if 条件表达式1:
      语句块1
  elif 条件表达式2:
      语句块2
  elif 条件表达式3:
      语句块3
  ...
  elif 条件表达式n:
      语句块n
  else:
      语句块(其他)
      
  ```

  - 说明
    - elif 子句可以有0个，1个或多个,
    - else 子句可以有0个或1个且只能放在最后
    - 它通过对表达式逐个求值直至找到一个真值（请参阅 [布尔运算](https://docs.python.org/zh-cn/3/reference/expressions.html#booleans) 了解真值与假值的定义）在子句体中选择唯一匹配的一个；然后执行该子句体（而且 [`if`](https://docs.python.org/zh-cn/3/reference/compound_stmts.html#if) 语句的其他部分不会被执行或求值）。 如果所有表达式均为假值，则如果 [`else`](https://docs.python.org/zh-cn/3/reference/compound_stmts.html#else) 子句体如果存在就会被执行。

- 练习

  ```python
  写一个程序，输入一个整数，用程序判断这个整数是正数，负数，还是零
  ```

- 练习2

  ```
  写一个程序， 06_input_score.py, 输入一个学生的成绩，如果成绩在 60~100 之间提示"及格", 否则提示不及格
  ```

- 练习3

  ```
  写一个程序， 07_input_score.py, 输入一个学生的成绩，
  如果成绩在 [0,60)  提示"不及格"
  如果成绩在 [60,80)  提示"及格"
  如果成绩在 [80,90)  提示"良好"
  如果成绩在 [90,100]  提示"优秀"
  如果是其他值，则提示“您的输入有误”
  ```

  > [ ] 表示包含 () 表示不包含

- if 语句也是语句，他可以嵌套到其他的复合语句中

  ```
  if xxxx:
       if yyyy > 0:
            print('.....')
       else:
            print("fjdsfdf")
  else:
      print("hello")
  ```

### pass 语句

- 作用

  用来填充语法空白

- 语法

    ```python
    pass
    ```

    

- 示例

  ```python
  # 如果 成绩在 0~100 什么都不做, 其他提示"您的输入有误"
  score = int(input('请输入成绩:'))
  if 0 <= score <= 100:
      pass
  else:
      print('您的输入有误!')
  ```

### 条件表达式

- 语法

  ```python
  表达式x if 条件表达式C else  表达式y
  ```

- 说明

  - 执行顺序,

    1. 先判断 条件表达式C 是真值还是假值，
    2. 如果 C是真值，则表达式x 执行并返回结果
    3. 如果 C是假值，则表达式y 执行并返回结果

    > 表达式 `x if C else y` 首先是对条件 *C* 而非 *x* 求值。 如果 *C* 为真，*x* 将被求值并返回其值；否则将对 *y* 求值并返回其值。

- 示例

  ```python
  >>> score = 69
  >>> "及格" if 60 <= score <= 100 else "不及格"
  '及格'
  >>> s = "及格" if 60 <= score <= 100 else "不及格"
  >>> print(s)
  及格
  
  ```

- 练习

  ```
  商场促销, 满100 减 20, 写一个程序，输入商品的价格，打印出实际支付金额
  ```

- 课后练习1

  ```
  BMI （Body Mass Index）  身体质量指数
  公式:
     BMI= 体重 (公斤) / 身高的平方
  如: 魏老师 体重 69公斤, 身高 1.73米
      BMI = 69 / 1.73 ** 2
  要求: 输入体重和身高，给出体重建议
  ```

  | BMI 值           | 结果     |
  | ---------------- | -------- |
  | BMI < 18.5       | 体重过轻 |
  | 18.5 <= BMI <=24 | 正常     |
  | BMI > 24         | 体重过重 |

- 课后练习2

  ```
  北京的出租车计价器的规则
      3公里以内 收费 13元 起步价
      3公里以外, 每公里加收 2.3 元(又称单价)
      超过 15 公里以后,  每公里再加收单价的 50% (1.15元) 的空驶费
  要求: 输入公里数，打印出费用的金额
  ```

  