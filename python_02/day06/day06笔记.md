## Python02(高级) day01笔记

- 表达式

    - 字面值

        - 字符串 str
        - 数字
            - 整数 int
            - 浮点数 float
            - 布尔类型bool
        - 容器
            - 列表 list
            - 元组 tuple
            - 字典 dict
            - 集合 set 和 固定集合 frozenset
            - 字节串 bytes 和 字节数组bytearray(后面在学)

    - 运算符

        - 算术运算符 

            ```python
             + - * / // % **
            ```

        - 比较运算符

            ```python
            <  >  <=   >=   ==   != 
            ```

        - 布尔运算符

            ```python
            and  or   not
            ```

        - 条件表达式

            ```python
            '及格' if score >= 60 else '不及格'
            ```

        - in / not in 运算符

            ```python
            '王昭君' in ['孙悟空', '赵云']
            ```

        - 索引和切片

            > 用于 str, list, tuple, bytes, bytearray

            ```python
            L = [1, 2, 3, 4, 5, 6]
            print(L[2])
            print(L[2:5])
            ```

    - 函数

        - 构造函数(函数名和类名相同的函数)

            ```
            str()  ---> ''        int() -->  0       
            float()      bool()       list()       tuple()      dict()
            set()      frozenset()
            ```

        - 函数

            ```python
            print()        input()      len(x)     max(x)     min(x)
            sum(x)        range()      type(x)
            ```

- 语句
    - 简单语句
        - 表达式语句
        - 赋值语句
        - del 语句
        - pass 语句
        - break 语句
        - contiue 语句
    - 复合语句
        - if 语句
        - while 语句
        - for 语句

> 所有的容器类都是可迭代对象，可以用 for 语句遍历其中的全部内容

## day06  笔记 

###  文字的编码

> Python3的字符串内部存储的是文字的UNICODE编码

- 字符串中存储的是什么?

    - 是一个图片(文字)对应的编码(code)

- 字符相关的函数

    |  函数  | 说明                               |
    | :----: | ---------------------------------- |
    | chr(x) | 根据x的UNICODE编码值得到对应的字符 |
    | ord(x) | 可以返回一个 字符的 UNICODE 编码值 |

    > 英文的编码值 详见 ASCII 编码
    >
    > ```shell
    > man ascii
    > ```

    > ord(x) 函数的返回值如果是0~127 则 x 是英文字符,中文字符串一个编码值一定大于128

-  示例

  ```
  >>> chr(98)
  'b'
  >>> chr(20000)
  '丠'
  >>> chr(20001)
  '両'
  >>> chr(20002)
  '丢'
  >>> chr(20003)
  '丣'
  >>> chr(20004)
  '两'
  >>> ord('两')
  20004
  >>> ord('A')   # 65
  >>> ord('B')   # 66    
  #     英文的编码值在 0~127 之间
  >>> ord('中')  # 20013
  
  ```

- 在python的交互模式下打印全世界的文字

    ```python
    for ch in range(65536):
        print(chr(ch), end='')
    ```

### 函数定义 function

- 什么是函数

  - 函数是可以重复执行的语句块，可以重复调用

- 作用

  用于封装语句块, 提高代码的重用性。

> 函数是面向过程编程的最小单位

#### def 语句

 - 作用

   用来定义（ 创建）函数

- 语法

  ```python
  def 函数名(形式参数列表):
      语句块
  ```

- 说明

  1. 函数的名字就是语句块的名称

  2. 函数名必须是标识符
  3. 函数名是一个变量，不要轻易对其赋值
  4. 函数有自己的名字空间，在函数外部不可以访问函数内部的变量，在函数内部可以访问函数外部的变量，但不能轻易对其改变
  5. 函数的形参列表如果不需要传入参数，形式参数列表可以为空

- 示例

  ```python
  # 定义一个函数，用 say_hello 变量绑定
  def say_hello():
      print("hello world!")
      print("hello tarena!")
      print("hello everyone!")
      
  # 定义一个函数，传入两个参数，让这个函数把最大的值打印到终端
  def mymax(a, b):
      if a > b:
          print("最大值是", a)
      else:
          print("最大值是", b)
  ```

#### 函数的调用

- 语法

  ```
  函数名(实际调用传递参数)
  ```

- 说明

  - 函数调用是一个表达式
  - 如果函数内没有return 语句，函数执行完毕后返回 None 对象

- 示例

  ```python
  # 调用
  say_hello()  # 调用一次
  say_hello()  # 调用第二次
  
  # 调用
  mymax(100, 200)
  mymax(999, 1)
  mymax('abc', 'cba')
  
  ```

  

- 练习

  ```
  写一个函数 myadd, 此函数传入两个参数， x, y 
  次函数的功能是打印  x + y 的和:
  def myadd(......):
       ....
  
  myadd(100, 200)   # 300
  myadd('ABC', '123')  # ABC123
  ```

### return 语句

- 语法

  ```python
  return [表达式]
  ```

  > 注: [] 代表 内部的内容可以省略

- 作用

  用于函数的内部，结束当前函数的执行，返回到调用此函数的地方，同时返回一个对象的引用关系

- 说明

  1. return 语句后面的表达式可以省略，省略后相当于 return None

  2. 如果函数内部没有 return 语句, 则函数执行完毕后返回None, 相当于在最后一条语句后有一条return None

- 示例

  ```python
  def say_hello():
      print("hello aaa")
      print("hello bbb")
      return 1 + 2
      print("hello ccc")
  
  r = say_hello()
  print(r)   # 3
  ```

- 练习2

  ```python
  写一个函数 myadd, 实现给出两个数，返回这两个数的和
  如:
      def myadd(x, y):
          ....
      a = int(input("请输入第一个数: "))
      b = int(input("请输入第二个数: "))
      print('您输入的两个数之和是:', myadd(a, b))
  ```


### 函数的调用传参

- 位置传参

  实际参数传递时，实参和形参 按`位置`来依次对应

- 关键字传参

  实际参数传递时，实参和形参 按`名称`依次对应

> 注: 位置传参要先于关键字传参

示例

> 见: 09_call_args.py

```pythoon
def myfun1(a, b, c):
    print('a=', a)
    print('b=', b)
    print('c=', c)

# 位置传参
myfun1(1, 2, 3)
# 关键字传参
myfun1(c=33, a=11, b=22)
# 位置传参要先于关键字传参
myfun1(111, c=333, b=222)   # 正确
```

-  问题

    ```
    写一个函数 myadd2, 实现给出四个数，返回这四个数的和
    如:
        def myadd(...):
            ....
        a = int(input("请输入第一个数: "))
        b = int(input("请输入第二个数: "))
        c = int(input("请输入第三个数: "))
        d = int(input("请输入第四个数: "))
        print('您输入的四个数之和是:', myadd(a, b, c, d))
    
        # 思考用什么方式实现:
    	print(myadd(1, 2))  # 3
    	print(myadd(1, 2, 3))  # 6
    	print(myadd(1, 2, 3, 4))  # 10
    ```

### 函数的形式参数定义方法

- 函数的缺省参数

  语法

  ```python
  def 函数名(形参名1=默认实参1, 形参名2=默认实参2, ... ):
       语句块
  ```

  说明

  缺省参数必须自右向左依次存在(即,如果一个参数有缺省参数，则其右侧的所有参数都必须有缺省参数)

  示例

  ```python
  def myadd4(a, b, c=0, d=0):
      return a + b + c + d
  print(myadd4(1, 2))
  print(myadd4(1, 2, 3))
  print(myadd4(1, 2, 3, 4))
  ```

  错误示例

  ```python
  >>> def myadd(a, b=0, c):   # 报错
  ...    pass
  
  ```

####  形参的定义

- 位置形参
- 星号元组形参(*args)
- 命名关键字形参
- 双星号字典形参（**kwargs）

##### 1) 位置形参

- 语法:

  ```python
  def 函数名(形参名1, 形参名2, ...):
      pass
  ```

##### 2) 星号元组形参

- 语法

  ```python
  def 函数名(*元组形参名):
      pass
  ```

- 作用

  收集多余的位置实参

  > 元组形参名 一般命名为args

- 示例

  ```python
  def myfunc2(*args):
      print("len(args)=", len(args))
      print('args=', args)
      
  myfunc2()           # args=()
  myfunc2(1, 2, 3)    # args=(1, 2, 3)
  
  def myfunc3(a, b, *args):
      print(a, b, args)
  
  myfunc3(1, 2)        # 1-->a, 2-->b, ()--->args
  myfunc3(1, 2, 3, 4)  # # 1-->a, 2-->b, (3, 4)--->args
  ```

  

##### 3）命名关键字形参

- 语法

  ```python
  def 函数名(*, 命名关键字形参1, 命名关键字形参2, ...):
      pass
  # 或者
  def 函数名(*args, 命名关键字形参1, 命名关键字形参2, ...):
      pass
  ```

- 作用

  强制，所有的参数都必须用关键字传参

- 示例

  ```python
  def myfunc4(a, b,*args, c, d):
      print(a, b, c, d)
  
  myfunc4(1, 2, d=4, c=3)   # 正确,c,d 必须关键字传参
  myfunc4(1, 2, 3, 4)   # 错误
  ```

##### 4）双星号字典形参

- 语法

  ```python
  def 函数名(**字典形参名):
      pass
  ```

- 作用

  收集多余的关键字传参

  > 字典形参名 最多有一个，
  >
  > 字典形参名 一般命名为 kwargs

- 示例

  ```python
  def myfunc5(**kwargs):
      print(kwargs)
  
  # {'name': 'tarena', 'age': 18}-->kwargs
  myfunc5(name='tarena', age=18) 
  
  ```

  

函数的形参定义方法说明

-  位置形参，星号元组形参，命名关键字参数，双星号字典形参，缺省参数可以混合使用。
- 函数的形参定义自左至右的顺序为：位置形参，星号元组形参，命名关键字参数，双星号字典形参

示例:

```python
def fn(a, b, *args, c, d, **kwargs):
    pass

fn(100, 200, 300, 400, c='C', e='E', d='D')
```

思考:

```python
print() # 函数是如何定义的呢:
def myprint(*args, sep=' ', end='\n'):
    pass
```

### 局部变量和全局变量

- 局部变量
  - 定义在函数内部的变量称为局部变量(函数的形参也是局部变量)
  - 局部变量只能在函数的内部使用
  - 局部变量在函数调用时才能够被创建，在函数调用之后会自动销毁
- 全局变量
  - 定义在函数外部，模块内部的变量称为全局变量
  - 全局变量, 所有的函数都可以直接访问(取值,但函数内部不能直接将其赋值改变)

- 局部变量示例

```python
>>> def fn(a, b):
...     c= 100
...     print(a, b, c)    # a, b, c三个都是局部变量
>>> fn(1, 2)
>>> print(a, b, c)   # 报错， 因为a,b,c 在调用后就销毁了
```

- 全局变量示例

```python
a = 100  # 全局变量
def fx(b, c):   # b,c 局部变量
    d = 400     # d 局部变量
    print(a, b, c, d)
    
fx(200, 300)
print(a)  # 100
print(b)  # 报错, 因为此时 b 不存在了
```

- 全局变量示例2

```python
a = 100  # 全局变量
def fx(b):
    a = 666  # 创建局部变量，不是改变全局变量
    c = 300
    print(a, b, c)  # 优先访问局部变量
    
fx(200)  # 666 200 300
print(a)  # 100

```

### global  语句 

- 问题

```python
# 如何用一个变量来记录一个函数调用的次数
count = 0

def hello(name):
    print('hello', name)
    count += 1  #  等同于 count = count + 1  # 如何让 此语句能改变全局变量而不是创建局部变量

hello('小张')
hello('小李')

print('您共调用hello函数', count, '次')  # 2 次
```

- 作用

  告诉python 的解释执行器， global 语句声明的一个或多个变量， 这些变量是全局变量

- 语法

  ```python
  global 变量名1, 变量名2, ....
  ```

- 示例

  ```python
  # 如何用一个变量来记录一个函数调用的次数
  count = 0
  
  def hello(name):
      global count  # 声明 global 是全局变量
      print('hello', name)
      count += 1  #  等同于 count = count + 1
  
  hello('小张')
  hello('小李')
  hello('小魏')
  
  print('您共调用hello函数', count, '次')  # 3 次
  ```

- global 说明 

  - 全局变量如果要在函数内部被赋值，则必须经过全局声明 global
  - 默认全局变量在函数内部可以使用，但只能取值，不能赋值
  - 不能先声明为局部变量，再用 global 声明为全局变量，此做法不符合语法规则
- 函数的形参已经时局部变量，不能用 global 声明为全局变量
  
> 错误示例
  
  ```python
  a = 100
  b = 200
  def fa(a):
      b = 20  # SyntaxError: name 'b' is assigned to before global declaration
      global b
      b = 222
  ```

### lambda 表达式(又称匿名函数)

- 语法

  ```python
  lambda  [函数的参数列表]: 表达式
  ```

- 作用

  - 创建一个匿名函数对象
  - 同 def 类似，但不提供函数名

- 说明

  lambda 表达式 的创建函数只能包含一个表达式

- 示例

  ```python
  def myadd(x, y):
      return x + y
  
  print('1 + 2 =', myadd(1, 2))  # 3
  
  # myadd 函数可以改写成 
  myadd2 = lambda x, y: x + y
  print('3 + 4 =', myadd2(3, 4))  # 7
  
  ```

- 示例2

  ```python
  >>> mymul = lambda a, b, c: a * b + c
  >>> 
  >>> mymul(3, 4, 5)
  17
  >>> def mymul2(a, b, c):
  ...     return a * b + c
  ... 
  >>> mymul2(3, 4, 5)
  17
  
  ```

  

- 课后练习1

```
写一个程序: 输入一个整数，代表树干的高度，打印如下圣诞树
请输入: 3
打印:
  *
 ***
*****
  $
  $
  $
请输入: 4
打印:
   *
  ***
 *****
*******
   $
   $
   $  
   $
```



- 课后练习2

```
写一个函数 myrange 实现和 range 一样的功能， 但返回列表
def myrange(start, stop=None, step=None):
	return range(start, stop, step)
    if stop == None:  # 这是一个参数
         return list(range(0, start))
	... 此处自己完成

for x in myrange(4):
    print(x)   # 0, 1, 2, 3

for x in myrange(3, 6):
    print(x)  # 3, 4, 5
for x in myrange(1, 10, 2):
    print(x)   # 1, 3, 5, 7, 9
    
```















