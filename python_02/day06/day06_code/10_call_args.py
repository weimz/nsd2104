def myfun1(a, b, c):
    print('a=', a)
    print('b=', b)
    print('c=', c)

# 位置传参
myfun1(1, 2, 3)
# 关键字传参
myfun1(b=22, c=33, a=11)
myfun1(c=666, a=444, b=555)
# 混合传参
myfun1(100, c=300, b=200)
# 以下做法是错误的？
myfun1(b=20, a=10, 30)