## python02 day06笔记

day10 回顾

- 正则表达式

    - re模块

- python 的第三方包

    - 在线安装

        ```shell
        pip3 install 包名
        pip install 包名
        ```

- pymysql 模块

- subprocess 模块

- 多线程 threading 模块

### paramiko 模块

此模块实现了 ssh 客户端的功能

ssh 命令 可以远程登录一台主机并远程操作这台主机

- 安装 paramiko

```shell
pip3 install paramiko
```

- 示例

```python
# 导入 paramiko 模块
import paramiko

# 创建一个paramko 客户端对象
ssh_clint = paramiko.SSHClient()

# 设置自动接受服务器的主机密钥
ssh_clint.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# 登陆远程主机
ssh_clint.connect('192.168.1.64',   # 远程主机的IP
                  username='root',  # 远程主机的用户名
                  password='root',  # 远程主机的密码
                  port=22  # ssh 的端口号
                  )

# 在此处操作远程主机
result = ssh_clint.exec_command('id root; id zhangsan')   # 在远程主机上执行命令
# print('len(result)=', len(result))  # result 绑定三个文件流对象的元组

stdout = result[1]   # 得到标准输出
stderr = result[2]   # 得到标准错误输出
print("标准输出：", stdout.read().decode())
print("标准错误输出：", stderr.read().decode())

ssh_clint.exec_command('mkdir 魏老师的专用文件夹')

# 关闭连接
ssh_clint.close()  # 相当于在ssh 的内部执行  exit 命令
```

### python 的电子邮件编程

- 两个内建模块

  ```
  email 模块, 用于准备邮件内容的模块
  smtplib 模块， 用于与邮件服务器连接并发送邮件的模块
  ```

- 使用 QQ 邮箱来发送电子邮件

  - 两步
    1. 配置QQ邮箱, 开启SMTP服务的设置, 获取验证码
    2. 编写 python 程序发送邮件

- 配置 QQ邮箱的SMTP 服务

  - 网址: mail.qq.com

  - 授权码:

    ```python
    aaaa bbbb cccc dddd
    ```

实验手册见: http://tedu.weimingze.com/static/py_library/python_email.html



### json 

json 全称( Javascript Object Notation) , 是一种轻量级的数据交互格式

json 是基于 Javascript 编程语言的格式

JSON 采用完全独立于编程语言的文本格式

标准JSON的合法符号：

-  {}(大括号)  

- "(双引号) 
- :(冒号)
-  ,(逗号)
-   \[ ](中括号)

Json 的数据类型

- 对象 object `{}`
- 数组 array  `[]`
- 字符串 string  `""`
- 数值 number (整数和浮点数)
- 布尔值 bool  (true/false)
- 空值: null

字典中的两个人

```
[
   {
       'name': 'weimingze',
       'age': 35
   },
   {
       'name': '小王',
       'age': 18,
       'score': [90, 100, 85]
   },
]
```

JSON中的两个人

```
[
   {
       "name": "weimingze",
       "age": 35
   },
   {
       "name": "小王",
       "age": 18,
       "score": [90, 100, 85]
   },
]
```

- json 和 Python 的类型和值

| Python      | Json             |
| ----------- | ---------------- |
| dict        | {}object(对象)   |
| list, tuple | []array数组      |
| str         | "" string 字符串 |
| int, float  | number 数字      |
| True/False  | true/false       |
| None        | null             |

- json 模块

  内建模块

```python
import json
```

json模块的四个常用函数

| 函数               | 说明                                             |
| ------------------ | ------------------------------------------------ |
| json.dump(obj, fw) | 把python 对象编码为json 字符串并写入文件fw中     |
| json.dumps(obj)    | 把python 对象编码为json 字符串并返回             |
| json.load(fr)      | 从文件流对象fr中读取json数据解码后返回python对象 |
| json.loads(json)   | 从json 字符串中解码Python 对象                   |

示例见:

```python
>>> infos = [
   {
       'name': 'weimingze',
       'age': 35,
       'gender': True,
       'score': None
   },
   {
       'name': '小王',
       'age': 18,
       'score': [90, 100, 85]
   },
]
>>> infos
[{'name': 'weimingze', 'age': 35, 'gender': True, 'score': None}, {'name': '小王', 'age': 18, 'score': [90, 100, 85]}]
>>> 
>>> import json
>>> s = json.dumps(infos)  # 将infos 绑定的对象转为json 的字符串
>>> s
'[{"name": "weimingze", "age": 35, "gender": true, "score": null}, {"name": "\\u5c0f\\u738b", "age": 18, "score": [90, 100, 85]}]'
>>> objs = json.loads(s)  # 将 json 的字符串，转为 python 的对象
>>> objs
[{'name': 'weimingze', 'age': 35, 'gender': True, 'score': None}, {'name': '小王', 'age': 18, 'score': [90, 100, 85]}]
>>> type(objs)
<class 'list'>
```

### API 

Application Programming Interface 应用程序接口。在Web中,API 通常指HTTP协议的接口

- 示例

  中国天气网查询天气情况的 API: http://www.weather.com.cn/data/sk/城市代码.html

- 城市代码:

  ```
  http://www.weather.com.cn/data/sk/101010100.html
  # 城市代码码表
  https://blog.csdn.net/wangqjpp/article/details/39957091
  ```

### requests 模块

- 作用

  发送HTTP 协议的请求，得到服务器响应的数据

  模拟浏览器的行为

- 安装方法:

  ```
  pip3 install requests
  ```

- HTTP  协议的请求方式:

  1. GET 请求

     相当于`查看` ,get请求可以获取网站的数据，请求参数通常跟在URL 的后面

  2. POST请求

     原意是`创建或者添加`， post请求通常用于提交表单或上传文件等

  3. 

  ```
  https://www.baidu.com/s?ie=utf-8&f=3&rsv_bp=1&rsv_idx=1&tn=baidu&wd=%E9%AA%B0%E5%AD%90&fenlei=256&rsv_pq=854e3d6f00038104&rsv_t=920aUYsotvE%2BTTV%2B4%2Fpf%2Fv1geYxMZTZFzMYCH5Fw%2Bg%2FZaWsk5SmRve30MRQ&rqlang=cn&rsv_enter=1&rsv_dl=ih_2&rsv_sug3=1&rsv_sug1=1&rsv_sug7=001&rsv_sug2=1&rsv_btype=i&rsp=2&rsv_sug9=es_2_1&inputT=7546&rsv_sug4=8033&rsv_sug=9
  ```

- 示例

  使用request模块获取网页数据

  ```python
  >>> import requests
  # 向 https://www.baidu.com/s?wd=weimingze 发送GET请求得到响应
  >>> r = requests.get('https://www.baidu.com/s?wd=weimingze')
  >>> r.text  # 得到响应内容的文本信息
  '<html>\r\n<head>\r\n\t<script>\r\n\t\tlocation.replace(location.href.replace("https://","http://"));\r\n\t</script>\r\n</head>\r\n<body>\r\n\t<noscript><meta http-equiv="refresh" content="0;url=http://www.baidu.com/"></noscript>\r\n</body>\r\n</html>'
  >>> r = requests.get('http://photo.qc188.com/upload/20197/31/24820/l_1_20190731145347532.jpg')
  >>> r.content  # 响应的字节串
  
  
  ```

- 使用request模块下载文件

  ```python
  >>> import requests
  >>> r = requests.get('http://photo.qc188.com/upload/20197/31/24820/l_1_20190731145347532.jpg')
  >>> r.content  # 响应的字节串
  # 下载图片
  >>> r = requests.get('http://photo.qc188.com/upload/20197/31/24820/l_1_20190731145347532.jpg')
  >>> 
  >>> with open('benz.jpg', 'wb') as fw:
  ...      fw.write(r.content)  # 将 响应的内容（bytes) 写入文件
  ... 
  155157
  
  ```

- 用request模块获取 中国天气网的json 数据

  ```python
  >>> import requests
  >>> url = 'http://www.weather.com.cn/data/sk/101010100.html'
  >>> r = requests.get(url)
  >>> r.content  # 返回字节串
  b'{"weatherinfo":{"city":"\xe5\x8c\x97\xe4\xba\xac","cityid":"101010100","temp":"27.9","WD":"\xe5\x8d\x97\xe9\xa3\x8e","WS":"\xe5\xb0\x8f\xe4\xba\x8e3\xe7\xba\xa7","SD":"28%","AP":"1002hPa","njd":"\xe6\x9a\x82\xe6\x97\xa0\xe5\xae\x9e\xe5\x86\xb5","WSE":"<3","time":"17:55","sm":"2.1","isRadar":"1","Radar":"JC_RADAR_AZ9010_JB"}}'
  >>> r.json()  # 将字节串，转化为python 的对象，相当于 json.loads(r.content.decode())
  {'weatherinfo': {'city': 'å\x8c\x97äº¬', 'cityid': '101010100', 'temp': '27.9', 'WD': 'å\x8d\x97é£\x8e', 'WS': 'å°\x8fäº\x8e3çº§', 'SD': '28%', 'AP': '1002hPa', 'njd': 'æ\x9a\x82æ\x97\xa0å®\x9eå\x86µ', 'WSE': '<3', 'time': '17:55', 'sm': '2.1', 'isRadar': '1', 'Radar': 'JC_RADAR_AZ9010_JB'}}
  >>> r.encoding  # 查看当前的字符编码
  'ISO-8859-1'
  >>> r.encoding = 'utf8' # 将编码改为 UTF-8
  >>> r.json()
  {'weatherinfo': {'city': '北京', 'cityid': '101010100', 'temp': '27.9', 'WD': '南风', 'WS': '小于3级', 'SD': '28%', 'AP': '1002hPa', 'njd': '暂无实况', 'WSE': '<3', 'time': '17:55', 'sm': '2.1', 'isRadar': '1', 'Radar': 'JC_RADAR_AZ9010_JB'}}
  
  ```

- requests.get ()为连接添加查询字符串使用params 字典完成

  ```python
  import requests
  
  url = 'https://www.sogou.com/web'
  # https://www.sogou.com/web?query=linux
  s = input('请输入查询的内容：')
  params = {'query': s}
  r = requests.get(url, params=params)  # 发出get请求，传入 ?query=s 查询字符串
      # 相当于请求：https://www.sogou.com/web?query=linux
  with open('sogou_' + s + '.html', 'wb') as fw:
      fw.write(r.content)   # 把 r.content 字节串写入文件
  
  
  # https://www.sogou.com/web?query=linux
  s = input('请输入查询的内容：')
  params = {'query': s}
  r = requests.get(url, params=params)  # 发出get请求，传入 ?query=s 查询字符串
      # 相当于请求：https://www.sogou.com/web?query=linux
  with open('sogou_' + s + '.html', 'wb') as fw:
      fw.write(r.content)   # 把 r.content 字节串写入文件
  ```
  
- request 可以通过 headers 传递请求头

  ```python
  url = 'https://www.sogou.com/web'
  params = {'query': 'linux'}
  headers = {
      'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
  }   # 带上 'User-Agent' 请求头,把自己伪装成浏览者
  r = requests.get(url, params=params, headers=headers)
  r.text  # 返回网页的内容
  ```

- 完整代码:

  ```python
  
  import requests
  
  url = 'https://www.sogou.com/web'
  # https://www.sogou.com/web?query=linux
  s = input('请输入查询的内容：')
  params = {'query': s}
  headers = {
      'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
  }   # 带上 'User-Agent' 请求头,把自己伪装成浏览者
  
  r = requests.get(url, params=params, headers=headers)  # 发出get请求，传入 ?query=s 查询字符串
      # 相当于请求：https://www.sogou.com/web?query=linux
  with open('sogou_' + s + '.html', 'wb') as fw:
      fw.write(r.content)   # 把 r.content 字节串写入文件
  
  
  ```
## python 实验

> 自己实现

### 阿里云开发者平台

网址: www.aliyun.com

实验手册参见: http://tedu.weimingze.com/static/py_library/python_ali_api.html

AppCode:

```
e3e8438c45584a768f11eef0a99f2b9a

```

- 示例代码

```python

import requests

# API 的地址
url = 'http://jisuqgtq.market.alicloudapi.com/weather/query'

# 请求头
headers = {
    # 根据API的要求，定义相对应的Content - Type
    'Content-Type': 'application/json; charset=UTF-8',
    # 权限指定 APPCODE
    'Authorization': 'APPCODE e3e8438c45584a768f11eef0a99f2b9a'
}

# 设置查询参数
params = {'citycode': '101010100'}  # 城市代码

r = requests.get(url, headers=headers, params=params)
# print('r.json=', r.text)  # json 数据
# r 即为相应对象其中包括天气信息
data = r.json()  # data 绑定字典
# print(data)
# 使用pprint 模块进行打印
import pprint
# pprint.pprint(data)

print('今晚的最低温度：')
pprint.pprint(data['result']['daily'][0]['night']['templow'])

```

### 使用钉钉机器人

实验手册:http://tedu.weimingze.com/static/py_library/python_dingtalk.html

webhook API

```
https://oapi.dingtalk.com/robot/send?access_token=5c55ff3b8f338041a4b04e82c13c2fea3ff3edf3844fde2d99261607aae54a57
```













