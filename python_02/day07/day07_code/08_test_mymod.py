# file: 08_test_mod.py
# 小李写的程序，小李想调用 小张写的 mymod.py
# 里的两个函数和两个字符串

# 1. 用  import 语句导入 mymod，  整体导入
import mymod
print(mymod.name1)  # audi
print(mymod.name2)  # tesla
mymod.mysum(100)
mymod.myadd(100, 200)

# 2. 用 from 模块 import xxx 语句导入  模块内部的某个属性
from mymod import name1, myadd

print('mymod.name1=', name1)
myadd(99, 1)

# 3. 用 from 模块 import * 语句导入某个模块内部全部的属性
from mymod import *
mysum(100)
print(name1)
print(name2)




