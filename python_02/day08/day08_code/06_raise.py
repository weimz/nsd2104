# 写一个函数, get_score 函数，读取用户输入的整数成绩,
# 成绩的正常值是0~100 之间， 要求, 如果不在0~100 之间
# 报 ValueError类型的错误

def get_score():
    x = int(input('请输入成绩:'))
    if 0 <= x <= 100:
        return x
    raise ValueError('用户输入的成绩不在 0～100之间！')

try:
    s = get_score()
    print('用户输入的成绩是：', s)
except ValueError:
    print('您的输入有误！')

