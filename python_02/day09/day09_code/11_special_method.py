class Book:
    def __init__(self, title, author):
        '''初始化方法，在实例化时自动调用'''
        self.title = title  # 标题
        self.author = author  # 作者
    def __str__(self): 
        return '%s的<<%s>>' % (self.author, self.title)
    def __call__(self, *args, **kwargs):
        print('__call__被调用, args=', args,
               'kwargs=', kwargs)

b1 = Book('Shell编程', '丁明一')
s = str(b1)  # 调用b1.__str__()
print(s)
b1(1,2,3, a=11, b=22, c=33)    # 调用b1.__call__()



