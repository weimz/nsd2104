# 导入 paramiko 模块
import paramiko

# 创建一个paramko 客户端对象
ssh_clint = paramiko.SSHClient()

# 设置自动接受服务器的主机密钥
ssh_clint.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# 登陆远程主机
ssh_clint.connect('localhost',   # 远程主机的IP
                  username='root',  # 远程主机的用户名
                  password='root',  # 远程主机的密码
                  port=22  # ssh 的端口号
                  )

# 在此处操作远程主机
result = ssh_clint.exec_command('id root; id zhangsan')   # 在远程主机上执行命令
# print('len(result)=', len(result))  # result 绑定三个文件流对象的元组

stdout = result[1]   # 得到标准输出
stderr = result[2]   # 得到标准错误输出
print("标准输出：", stdout.read().decode())
print("标准错误输出：", stderr.read().decode())

ssh_clint.exec_command('mkdir 魏老师的专用文件夹')

# 关闭连接
ssh_clint.close()  # 相当于在ssh 的内部执行  exit 命令