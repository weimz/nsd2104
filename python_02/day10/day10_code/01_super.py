
class A:
    def say_hello(self):
        print('Hello A')

class B(A):
    def say_hello(self):
        print("hello B")
        # self.say_hello() 无限递归调用
        # 能否在此方法内调用 A类里面的 say_hello()
        # 方法1
        # A.say_hello(self)
        # 方法2  ,super() 会把self 看成是A类的对象
        super().say_hello()

b = B()
b.say_hello()  # hello B    Hello A