# BMI （Body Mass Index）  身体质量指数
# 公式:
#    BMI= 体重 (公斤) / 身高的平方
# 如: 魏老师 体重 69公斤, 身高 1.73米
#     BMI = 69 / 1.73 ** 2
# 要求: 输入体重和身高，给出体重建议

weight = float(input('请输入体重(kg): '))
height = float(input('请输入身高(m)：'))

bmi = weight / (height ** 2)
print('bmi=', bmi)
if bmi < 18.5:
    print('体重过轻！')
elif 18.5 <= bmi <= 24:
    print('正常体重')
else:  # elif bmi > 24:
    print('体重过重！！')


