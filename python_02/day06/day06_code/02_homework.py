# 写一个程序，输入任意行文字，当输入为空字符串时结束输入，将所有的字符串以最长的字符串宽度打印如下方框
# 请输入: hello
# 请输入: hello world
# 请输入: <回车输入结束>
# +-------------+
# |    hello    |
# | hello world |
# +-------------+

L1 = []   # 用于用户输入的字符串
L2 = []   # 用于存储字符的长度
while True:
    s = input('请输入: ')  # hello中国    # 宽度为9， len(s)  --> 7
    if s == '':
        break
    L1.append(s)
    L2.append(len(s))
max_width = max(L2)  # 求最大的长度
# 打印第一行
line1 = '+-' + '-' * max_width + '-+'
print(line1)
# 再打印其他的 len(L1) 行
for s in L1:
    aline = '| ' + s.center(max_width) + ' |'
    print(aline)
# 再打印最后一行
print(line1)
# +-------------+
# |    hello    |
# | hello world |
# +-------------+


