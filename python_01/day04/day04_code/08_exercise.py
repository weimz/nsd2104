# 写一个程序，输入一个任意长度的字符串，用for 语句找出这个字符串中有几个英文的等号(=)
# 注： 不允许使用 str.count 方法
# 如:
#    请输入: a = b = c = 100
#    结果: 3

s = input('请输入: ')
count = 0   # 用于计数
for ch in s:
    if ch == '=':
        count += 1

print('结果是:', count)