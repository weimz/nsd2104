
a = 100  # 全局变量
def fx(b, c):   # b,c 局部变量
    d = b + c     # d 局部变量
    a = 999  # 创建一个名字为a 的局部变量
    print(a, b, c, d)

fx(200, 300)
print('全局的 a=', a)

