## python02 day02笔记

- tython2_day02 回顾

    - 文字的编码

        ```
        #  chr(x)     
        s = chr(20000)
        # ord(x)  # 给出文字，返回编码
        code = ord('中')
        ```

    - 函数

        - 语句

            - def 语句

                ```python
                def 函数名(位置形参1, 位置形参2, ..., *args, 命名关键字形参1, 命名关键字形参2, ..., **kwargs):
                    语句块
                
                # 调用
                函数名(位置实参1, 位置实参2, 位置实参3, ..., 关键字实参1, 关键字实参2, 关键字实参3, ....)
                ```

            - return 语句

            - global 语句

        - 表达式

            - lambda 表达式

    - 局部变量和全局变量



> python02 day02 笔记

### id(x) 函数

- 作用

  返回一个对象在内存中的地址

- 示例

```python
>>> L = list(range(10))
>>> id(L)
140416266741832
>>> L2 = list(range(10))
>>> id(L2)
140416266741896
>>> L
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> L2
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> L is L2
False
```

### is / is not 运算符

- is 作用

  判断两个对象的id ，如果为同一个地址，返回True, 否则返回False

  is not 返回值与 is 相反

- 示例

```python
>>> weimingze = "魏眀择"
>>> weilaoshi = weimingze    # 两个变量同时绑定同一个字符串"魏明择"
>>> 
>>> weilaoshi is weimingze  # id(weilaoshi) == id(weimingze) 返回True
True
>>> laowei = "魏" + "眀择"  # 创建一个新的字符串"魏明择"
>>> 
>>> weimingze is laowei  # id(weiweimingze) == id(laowei)  # 返回False
False
```

### None 对象

python 内部只有一个None 对象

通常判断一个变量是否绑定None,用 is 运算符(很少用 == 运算符)

### 函数式编程

用一系列函数来解决问题

```
求 1 + 2 + 3 + 4 + ..... 99 + 100 的和
# 用函数式编程
print(sum(range(1, 101)))
```

- python 中的常用高阶函数

  > 高阶函数是指，函数的参数可以传入函数

  ```pythono
  >>> def fb(a, b):
  ...      print(a)
  ...      print(b)
  ...      print(b(a))  # b([1, 2, 3, 4])  # 等用于 sum([1, 2, 3, 4])
  ... 
  >>> fb([1, 2, 3, 4], sum)
  [1, 2, 3, 4]
  <built-in function sum>
  10
  
  ```

- python 中常用的函数

  - map  函数
  - filter 函数
  - sorted  函数

| 函数                                       | 说明                                                         |
| ------------------------------------------ | ------------------------------------------------------------ |
| map(func, 可迭代对象1, ...)                | 返回一个可迭代对象, 此可迭代对象的每个数据用函数func 处理后返回 |
| filter(func, 可迭代对象)                   | 返回一个可迭代对象, 此可迭代对象的数据用func 函数进行筛选后过滤 |
| sorted(可迭代对象, key=None,reverse=False) | 返回一个列表, 所有的可迭代对象用key 的返回值进行排序         |

- map函数

```python
>>> def power2(x):
...     return x ** 2
... 
>>> L = [8, 5, 1, 3]  # 求出 列表内 平方并打印
>>> for x in map(power2, L):   # 把 power2  函数和 列表 L 交给 map 函数
...     print(x)
... 
64
25
1
9
```

- filter 函数

```python
>>> def is_odd(x):
...      return x % 2 == 1
... 
>>> is_odd(3)
True
>>> is_odd(10)
False
>>> L = [8, 5, 2, 1, 8, 6, 9, 7]
>>> # 把列表里所有的奇数取出来
>>> L2 = list(filter(is_odd, L))
>>> L2
[5, 1, 9, 7]
```

- 练习

```python
写一个程序。输入一些学生的成绩，当输入负数时结束, 把不及格的学生成绩打印出来
```

- sorted 函数

```python
>>> L = [5, -2, 4, -3, 1]
>>> sorted(L)  # 默认是升序排序
[-3, -2, 1, 4, 5]
>>> sorted(L, reverse=True)  #  降序排序
[5, 4, 1, -2, -3]
>>> abs(-2)  # abs 用来返回一个 数的绝对值
2
>>> abs(2) 
2
>>> sorted(L, key=abs)  # [1, -2, -3, 4, 5]  # 用每个数据 abs 的返回值作为排序依据进行排序
[1, -2, -3, 4, 5]
```

- 练习

```python
写一个程序。输入一些学生的成绩，当输入负数时结束, 把前三名的学生成绩放在一个列表中
```

### 模块

一个.py 文件就是一个模块

模块是含有一些列数据，函数，类等的程序

- 作用

  把相关功能的函数等放在一起有利于管理，有利于多人合作开发

- 模块的分类
  1. 内置模块（在python3 程序内部，可以直接使用）
  2. 标准库模块(在python3 安装完后就可以使用的 )
  3. 第三方模块（需要下载安装后才能使用）
  4. 自定义模块(用户自己编写)

  > 模块名如果要给别的程序导入，则模块名必须是 标识符
  
- 实例

```python
# file: mymod.py
'''
小张写了一个模块，内部有两个函数，两个字符串
... 此处省略 200字
'''

name1 = 'audi'
name2 = 'tesla'

def mysum(n):
    '''
    此函数用来求和
    by weimingze
    '''
    print("正在计算， 1 + 2 + 3 + .... + n的和 ")


def get_name():
    return "tarena"

```

>  调用模块

```python
# file: test_mod.py
# 小李写的程序，小李想调用 小张写的 mymod.py 里的两个函数和两个字符串

# 用import 语句导入模块
import mymod

print(mymod.name1)  # Audi
print(mymod.name2)    # tesla

mymod.mysum(100)  # 调用 mymod 模块里的 mysum 函数
print(mymod.get_name())   # 'tarena'
```

#### import 语句

> 导入

- 语法

1. ```python
   import 模块名  [as 模块新名字1]
   ```

   > 导入一个模块到当前程序

2. ```python
   from 模块名 import 模块属性名 [as 属性新名]
   ```

   > 导入一个模块内部的部分属性到当前程序

3. ```python
   from 模块名 import *
   ```

   > 导入一个模块内部的全部属性到当前程序

- 示例

  ```python
  import mymod
  mymod.mysum(10)   # 要加模块名
  
  from mymod import get_name
  print(get_name())   # 调用get_name 时不需要加 "模块名."
  
  from mymod import *   
  print(get_name())
  print(name2)
  ```

- 模块的内部属性

  ```
  __file__  绑定 模块的路径
  __name__  绑定模块的名称
         如果是主模块（首先启动的模块）则绑定 '__main__'
         如果不是主模块则 绑定 xxx.py 中的 xxx 这个模块名
  ```

- 示例 

  ```python
  见： test_mymod.py  和 mymod.py
  ```

python 的第三方模块

> https://pypi.org/

#### random 模块

> 生成随机数
>
> 文档位置: https://docs.python.org/zh-cn/3/library/random.html

```python
>>> import random
>>> random.randint(1, 6)  # random.randint(a,b) 生产 a~b的随机整数
3
>>> random.randint(1, 6)
4
>>> random.random()   # random.random  生成包含0 但不包含1 的浮点数
0.5884109388439075
>>> random.choice("ABCD")    # 从一个序列中，随机返回一个元素
'C'
>>> random.choice("ABCD")
'B'
>>> L = [1, 2, 3, 6, 9]
>>> random.choice(L)
6
>>> random.shuffle(L)   # random.shuffer(x)  # 把列表X 打乱
>>> L
[1, 6, 2, 9, 3]
```

- 练习

```
写一个程序， 生产6位由数字组成的随机密码
```

#### time 模块

> https://docs.python.org/zh-cn/3/library/time.html

时间戳:从 1970年1月1日 0:0:0 UTC 时间 开始计时到现在的秒数

UTC 时间 : 世界协调时间

struct_time 用 含有9个元素的元组来表示时间

```python
>>> import time
>>> time.time()   # 返回当前时间的时间戳
1617117219.0382686
>>> time.ctime()    #返回当前的UTC 时间的字符串
'Tue Mar 30 23:14:48 2021'
>>> t1 = time.localtime()   # 返回当前的本地时间元组
>>> t1
time.struct_time(tm_year=2021, tm_mon=3, tm_mday=30, tm_hour=23, tm_min=18, tm_sec=22, tm_wday=1, tm_yday=89, tm_isdst=0)
>>> t1.tm_year
2021
>>> t1.tm_yday
89
>>> time.sleep(3)  # time.sleep(n)  # 让程序睡眠 n 秒
>>> time.strftime("%Y-%m-%d", t1)   # 格式化时间
'2021-03-30'
>>> time.strftime("%y-%m-%d", t1)
'21-03-30'
>>> time.strftime('%Y-%m-%d %H:%M:%S', t1)
'2021-07-21 17:37:41'
    # 用时间元组来创建一个自定义的时间
>>> t2 = time.struct_time ( (2021,1, 1, 10, 11, 20, 0, 0, 0) )
```

#### datetime 模块

> https://docs.python.org/zh-cn/3/library/datetime.html

```python
>>> import datetime
>>> d1 = datetime.datetime.now()  # 返回当前的时间
>>> d1
datetime.datetime(2021, 3, 30, 23, 32, 7, 342559)
>>> d1.year
2021
>>> d1.year, d1.month, d1.day, d1.hour, d1.minute, d1.second, d1.microsecond  # 用 datetime 的各个属性可以得到 具体的信息
(2021, 3, 30, 23, 32, 44, 757673)
>>> d1.strftime("%Y-%m-%d")
'2021-03-30'

# 计算时间差
>>> delta_time = datetime.timedelta(days=2, hours=1)  # 生成 2天1小时后的时间差
>>> delta_time
datetime.timedelta(2, 3600)
>>> t1 = datetime.datetime.now()  # 得到当前时间
>>> t1
datetime.datetime(2021, 3, 30, 23, 39, 26, 863109)
>>> t1 + delta_time  # 计算 未来时间
datetime.datetime(2021, 4, 2, 0, 39, 26, 863109)

```



### 异常

- 作用

  用作信号通知，通知上层调用者有错误产生需要处理

#### try 语句

- 语法

```python
try:
    可能发生异常的语句块
except 错误类型1 [as 变量名1]:
    异常处理语句块1
except 错误类型2 [as 变量名2]:
    异常处理语句块2
...
except 错误类型n [as 变量名n]:
    异常处理语句块n
except:
    异常处理语句块other
else:
    未发生异常的语句
finally:
    最终的处理语句

```

- 作用

  尝试捕获异常，得到异常通知，将程序由异常状态变为正常状态

- 说明

  except 子句可以有 1个或多个

  except: 不给错误类型，可以匹配全部的错误类型

  else 子句里的语句会在 没有错误发生时执行，当处于异常时不执行

  finally 子句里的语句，无论何时都执行

- 示例

```python
try:
    x = int(input("请输入一个整数："))
    print('x=', x)
except ValueError:
    print('您的输入不能转成整数')

print("程序结束")
```



#### raise 语句

- 问题

  ```python
  # 写一个函数, get_score 函数，读取用户输入的整数成绩, 
  # 成绩的正常值是0~100 之间， 要求, 如果不在0~100 之间
  # 报 ValueError类型的错误
  def get_score():
      x = int(input('请输入成绩:'))
      if 0 <= x <= 100:
          return x
      raise ValueError
      
  ```

- 语法

  ```python
  raise 异常类型
  或
  raise 异常对象
  ```

- 作用

  - 抛出一个错误，让程序进入异常状态
  - 发送错误通知给调用者

- 示例:

```python
# 写一个函数, get_score 函数，读取用户输入的整数成绩,
# 成绩的正常值是0~100 之间， 要求, 如果不在0~100 之间
# 报 ValueError类型的错误
def get_score():
    x = int(input('请输入成绩:'))
    if 0 <= x <= 100:
        return x
    # raise ValueError
    raise ValueError('用户输入的成绩不在 0～100 之间')


try:
    score = get_score()
    print(score)
except ValueError as err:
    print("成绩输入有误 err=", err)
```



- 异常类型的可选种类

  > 详见：

  ```python
  >>> help(__builtins__)
  ```

- 课后练习1

  ```python
  写一个猜拳游戏: 石头，剪刀，布， 
  让电脑随机生成一个，
  你的输入如下:
      0)  石头
      1)  剪刀
      2)  布
      q)  退出
      请选择: 0
     电脑出的是 布 ，你输了
  循环输入，知道输入q 为止
  ```

- 课后练习2

  ```
  写一个猜数字游戏 让电脑随机生成一个 0 ~ 100 的整数让用来猜
  如果 您输入的数大于电脑生产的数，提示：“您猜大了”， 继续猜
  如果 您输入的数小于电脑生产的数，提示：“您猜小了”， 继续猜
  当 您输入的数等于电脑生产的数，提示："恭喜您猜对了" 打印猜的次数后退出程序
  ```

  

  

  

