
L =     [5, -2, 4, -3, 1]
# 参考值 [5, 2, 4, 3, 1]

L2 = sorted(L)  # 升序
print('升序后', L2)
L3 = sorted(L, reverse=True)  # 降序
print('降序后', L3)

# 能否按着绝对值进行排序成 [1, -2, -3, 4, 5] ???
# L4 = sorted(L, key=abs)  # key=lambda x: -x if x < 0 else x
L4 = sorted(L, key=lambda x: -x if x < 0 else x)
print('按绝对值排序后', L4)    # 课间休息： 15：10 回来

L5 = sorted(['hello', '123', 'beijing'])
print(L5)
L6 = sorted(['hello', '123', 'beijing'], key=len)
print(L6)
L7 = sorted(['hello', '123', 'beijing'], key=len, reverse=True)
print(L7)

def fxxx(x):
    if x < 0:
        return True
    else:
        return False

lambda x: True if x < 0 else False
