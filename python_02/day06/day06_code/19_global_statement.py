# 如何用一个变量来记录一个函数调用的次数

count = 0

def hello(name):
    # global name  # 函数的形参已经时局部变量，不能用 global 声明为全局变量
    print('hello', name)
    global count  # 声明 count 是全局变量，不是局部变量
    count = count + 1  # count += 1

hello('小张')
hello('小李')
# ...
hello('老魏')

print('您共调用hello函数', count, '次')  # 2 次