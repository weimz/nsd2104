# 写一个程序，输入一个整数，用程序判断
# 这个整数是正数，负数，还是零

number = int(input('请输入一个整数：'))
if number > 0:
    print(number, '是正数')

if number < 0:
    print(number, '是负数')

if number == 0:
    print(number, '是零')