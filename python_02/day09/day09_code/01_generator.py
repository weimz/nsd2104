# 定义一个生成器函数, 有 yield 的函数调用后回返回生成器对象

def myrange(stop):
    i = 0
    while i < stop:
        # print('i=', i)
        yield i  # 生成i
        i += 1
# result = myrange(5)
# print(result)
for x in myrange(5):
    print('x=', x)




