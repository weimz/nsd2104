
# 写一个程序，求
# 1 + 4 + 9 + 16 + .... 10000
# 1**2 + 2**2 + 3**2 + .... 100**2
# 的和
def power2(x):
    return x ** 2

# range(1, 5)              [1, 2, 3, 4]
# map(power2, range(1, 5)) [1, 4, 9, 16]
for x in map(power2, range(1, 5)):
    print(x)

print(sum(map(power2, range(1, 101))))
print(sum(map(lambda x:x**2, range(1, 101))))


