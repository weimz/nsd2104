# 写程序，输入一个字符， 打印字符串的如下内容
# 1. 打印这个字符串的第一个字符
# 2. 打印这个字符串的最后一个字符串
# 3. 如果这个字符串的长度是 奇数，则打印中间这个字符
#    用 len(x) 求字符串的长度

content = input('请输入文字：')
print("第一个字符是", content[0])
print("最后一个字符是", content[-1])
length = len(content)
if length % 2 == 1:
    print('中间的字符是:', content[int(length // 2)])

