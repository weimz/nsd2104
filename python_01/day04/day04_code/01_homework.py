# 写程序，输入一个整数n , 代表结束的整数， 计算
#         1 + 2 + 3 + 4 + ..... + n 的和
# 请输入: 100
# 5050
# # 提示：
# he = 0  # 用一个变量来记录这个和

n = int(input('请输入一个整数: '))
i = 1
total = 0   # total 变量，用于累计 i 的值
while i <= n:
    total += i  # print(i)
    i += 1

print('和是', total)  # tab-->
                      # shift + tab   <---