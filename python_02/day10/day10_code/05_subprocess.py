
import subprocess
# 启动一个shell进程，让此进程执行 'ls ~'命令
result = subprocess.run(
    'ls ~',
    shell=True, # 在子进程中运行此命令
    stdout=subprocess.PIPE  # 指明在标准输出保存到stdout属性中
)
print('您执行的命令是', result.args)
print('此程序的返回值是', result.returncode)
print('此程序的输出是', result.stdout.decode())