
signals = ['石头', '剪刀', '布']

try:    # 课间休息： 11：15 回来
    x = int(input("请输入一个整数："))  # int('一百')
    print('您猜的是', signals[x])
    print('hello world!')
except ValueError as err:
    print('int 函数发生了错误，错误已经解决，err=', err)
    x = 0
except IndexError:
    print('您猜的数字超出了范围，无法正确执行， 请重新输入！')

print('x=', x)
print('程序正常结束')