# 写一个函数 myadd2, 实现给出四个数，返回这四个数的和
# 如:
#     def myadd(...):
#         ....
#     a = int(input("请输入第一个数: "))
#     b = int(input("请输入第二个数: "))
#     c = int(input("请输入第三个数: "))
#     d = int(input("请输入第四个数: "))
#     print('您输入的四个数之和是:', myadd(a, b, c, d))


def myadd(a, b, c=0, d=0):
    return a + b + c + d

# a = int(input("请输入第一个数: "))
# b = int(input("请输入第二个数: "))
# c = int(input("请输入第三个数: "))
# d = int(input("请输入第四个数: "))
# print('您输入的四个数之和是:', myadd(a, b, c, d))

print(myadd(1, 2))
print(myadd(1, 2, 3))
print(myadd(1, 2, 3, 4))