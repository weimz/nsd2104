
# 写一个程序，把列表 L 中的奇数 挑选出来，形成一个新的列表 L2

L = [8, 5, 2, 1, 8, 6, 9, 7]
# 3. 用lambda 和 list 函数组合使用
L2 = list(filter(lambda x: x % 2 == 1, L))

# 2. 用 filter 函数实现
# L2 = []
# def is_odd(x):
#     return x % 2 == 1
# for x in filter(is_odd, L):
#     L2.append(x)

# 1. 用循环来实现
# for x in L:
#     if x % 2 == 1:
#         L2.append(x)

print("L2=", L2)


