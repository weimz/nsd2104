# 打印 九九 乘法表
# 1x1=1
# 1x2=2 2x2=4
# 1x3=3 2x3=6 3x3=9
# ....
# 1x9=9 ........ 9x9=81

# 创建一个函数, 给出一个整数,打印一行,如给出3,则打印
# 1x3=3 2x3=6 3x3=9
def print_99_aline(n):
    for i in range(1, n+1):
        print('%dx%d=%d' % (i, n, i*n), end=' ')
    print()

def print_99_table():
    for line in range(1, 10):
        print_99_aline(line)

if __name__ == '__main__':
    print_99_table()

