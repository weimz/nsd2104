

class Car:
    def run(self, speed):
        print(self.color, '的', self.brand, '正在以',
            speed, 'km/h的速度行驶')

car1 = Car()  # 创建第一辆汽车(红色 比亚迪)
car2 = Car()  # 创建第二辆汽车(白色 宾利)
car1.color = '红色'    # 颜色
car1.brand = '比亚迪'  # 品牌 
car2.color = '白色'    # 颜色
car2.brand = '宾利'    # 品牌 


# 第一种调用实例方法的方式
Car.run(car1, 120)
Car.run(car2, 230)
# 第二种调用实例方法的方式
car1.run(160)
car2.run(330)





