

## 《PyCharm 安装指导手册》



> 安装环境 Redhat 8

[TOC]

### 1. PyCharm 简介

PyCharm 是用来编写 Python 程序文件最好用的集成开发环境(IDE)之一

PyCharm 用两种版本:

1. 付费的专业版
2. 免费的社区版

> 免费的社区版只能写纯python代码；
>
> 付费的专业版功能更强大，它提供了30天的免费试用





### 2. 下载PyCharm 专业版 安装包

- 下载地址：

  https://download.jetbrains.com/python/pycharm-professional-2020.1.1.tar.gz

使用 Linux 命令下载:

```shell
[root@localhost ~]# cd ~
[root@localhost ~]# wget https://download.jetbrains.com/python/pycharm-professional-2020.1.1.tar.gz
```



### 3. 安装PyCharm

```shell
# 解压缩数据包
[root@localhost ~]# tar -xzf pycharm-professional-2020.1.1.tar.gz 
[root@localhost ~]# mkdir -p /root/bin
[root@localhost ~]# mv pycharm-2020.1.1 /root/bin/

```

### 4. 运行PyCharm

```shell
# 运行PyCharm
[root@localhost ~]# /root/bin/pycharm-2020.1.1/bin/pycharm.sh 
```

> 如果能够看到如下界面说明 PyCharm 已经安装成功

### 5. 首次启动PyCharm需要配置下选项



![1597112566928](./images/1.png)

![1597112601318](./images/2.png)

![1597112661506](./images/3.png)

![1597112720305](./images/4.png)

### 6. 用PyCharm 创建第一个项目

![1597112780935](./images/5.png)

![1597112780935](./images/6.png)

![1597112893182](./images/7.png)

![1597112940120](./images/8.png)

![](images/9.png)

> 至此新项目创建完毕，可以写Python 的代码并运行了

### 7. 为PyCharm添加快速启动按钮程序快捷方式

![](images/10.png)

![](images/11.png)

![](images/12.png)

> 用上述快捷方式可以快速启动PyCharm,可以 运行
>
> ```
> /root/bin/pycharm-2020.1.1/bin/pycharm.sh 
> ```
>
> 启动PyCharm

### 8. 修改Python 的设置(可选)

![1597113001279](./images/1597113001279.png)

![1597113039101](./images/1597113039101.png)

插件中搜索并安装chinese...（中文菜单），rainbow brackets，Translation（翻译）

![](./images/1597113275498.png)

![1597113396754](./images/1597113396754.png)

### 9. 创建python 的 .py 文件并运行程序

![](images/17.png)

填写文件名

![](images/18.png)

运行hello.py

> 第一次运行需要等待很长时间才能)看到如下菜单

![](images/19.png)

看到如下结果即为成功

![](images/20.png)



##



