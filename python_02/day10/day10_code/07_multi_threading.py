
import threading
import time

def say_hello():
    for x in range(10):
        print("hello!!!")
        time.sleep(1)

def say_world():
    for y in range(10):
        print('world!!!')
        time.sleep(1)

# 使用单进程执行
# say_hello()  # 花费 10秒
# say_world()  # 花费 10秒

# 使用多进程执行
t1 = threading.Thread(target=say_hello)
t1.start()
t2 = threading.Thread(target=say_world)
t2.start()

print('主程序结束')