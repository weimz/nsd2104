# file: pymysql_screate_table.py

# 导入 pymysql 包
import pymysql

# 连接数据库

conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',
    db='nsd2104',   # 制定操作哪一个数据库
    charset='utf8'   # 制定操作的字符集
)

# 操作数据库
# 需要使用 游标来操作数据库
cursor = conn.cursor()  # 创建游标

# 制定要操作的 SQL 语句
create_dep = '''CREATE TABLE departments(
id INT,
dep_name VARCHAR (20),
PRIMARY KEY(id)
)'''

# 使用游标 来执行 SQL 语句
cursor.execute(create_dep)  # 写入 SQL 语句
conn.commit()  # 提交SQL 语句到 服务器去执行

# 如果不再执行SQL 语句则 需要关闭游标
cursor.close()

# 操作完数据库，断开连接
conn.close()  # > quit;