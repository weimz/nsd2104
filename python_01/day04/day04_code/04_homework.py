# 写一个程序，输入三行文字， 按最长的一行， 打印如下图形方框
# 请输入: hello
# 请输入: welcome to beijing
# 请输入: aaaaaaa
# 打印:
# +--------------------+
# |       hello        |
# | welcome to beijing |
# |       aaaaaaa      |
# +--------------------+
# 提示: str.center(width)


s1 = input('请输入第一行: ')  # ctrl + d
s2 = input('请输入第二行: ')
s3 = input('请输入第三行: ')

# 求取三个字符的长度：
len1 = len(s1)
len2 = len(s2)
len3 = len(s3)


# 第三方法
maxlen = max(len1, len2, len3)

# 第二方法
maxlen = len1

if len2 > maxlen:
    maxlen = len2

if len3 > maxlen:
    maxlen = len3
print('最大值是', maxlen)
# 第一方法
# if len1 > len2:
#     # len1 大
#     if len1 > len3:
#         maxlen = len1  # maxlen用于保存最大值
#     else:
#         maxlen = len3
# else:
#     # len2 大
#     if len2 > len3:
#         maxlen = len2  # maxlen用于保存最大值
#     else:
#         maxlen = len3

line1 = '+-' + '-' * maxlen + '-+'

# 打印第一行
print(line1)
# 打印中间的三行
line2 = '| ' + s1.center(maxlen) + ' |'
print(line2)
line3 = '| ' + s2.center(maxlen) + ' |'
print(line3)
line4 = '| ' + s3.center(maxlen) + ' |'
print(line4)

# 打印最后一行
print(line1)


