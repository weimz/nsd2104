# 写一个猜拳游戏: 石头，剪刀，布，
# 让电脑随机生成一个，
# 你的输入如下:
#     0)  石头
#     1)  剪刀
#     2)  布
#     q)  退出
#     请选择: 0
#    电脑出的是 布 ，你输了
# 循环输入，知道输入q 为止

import random

def show_menu():
    print(' 0)  石头 ')
    print(' 1)  剪刀 ')
    print(' 2)  布 ')
    print(' q)  退出 ')

signals = ['石头', '剪刀', '布']

def compare(computer_signal, your_signal):  # 用于评判
    '''
    :param computer_signal: 电脑的手势
    :param your_signal: 有的手势
    :return: None
    '''
    if computer_signal == your_signal:
        print('平手')
    elif computer_signal == '石头' and your_signal == '剪刀':
        print('电脑出的是', computer_signal, '你输了')
    elif computer_signal == '石头' and your_signal == '布':
        print('电脑出的是', computer_signal, '你赢了')
    elif computer_signal == '剪刀' and your_signal == '布':
        print('电脑出的是', computer_signal, '你输了')
    elif computer_signal == '剪刀' and your_signal == '石头':
        print('电脑出的是', computer_signal, '你赢了')
    elif computer_signal == '布' and your_signal == '剪刀':
        print('电脑出的是', computer_signal, '你赢了')
    elif computer_signal == '布' and your_signal == '石头':
        print('电脑出的是', computer_signal, '你输了')

def main():  # 此函数用于运行主程序
    while True:
        show_menu()
        s = input('请选择: ')
        if s == 'q':
            return
        computer_select = random.choice(signals)
        your_select = signals[int(s)]
        compare(computer_select, your_select)

main()