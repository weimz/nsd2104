

# 高阶函数
def fa(a, b):  # fa的b参数必须接收一个函数作为参数，fa是高阶函数
    x = b(a)
    print('x=', x)

fa([1, 2, 3, 4], sum)
fa([1, 2, 3, 4], len)
fa([1, 2, 3, 4], lambda L:L[1] + L[2])

def fx(L):
    return L[0] + L[-1]
fa([1, 2, 3, 4], fx)