
for x in range(5):
    if x % 2 == 1:
        continue
    print(x)
print('======================================')
x = 0
while x < 5:
    if x % 2 == 1:
        x += 1
        continue
    print(x)
    x += 1