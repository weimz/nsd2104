
# 在数据库 nsd2104 中创建一个数据表 departments
# 此表用与存储一个公司部分的信息

# 导入pymysql
import pymysql

# 连接数据库
conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',   # 改为自己的密码
    # 'create databases nsd2104 default charset utf8'
    db='nsd2104',         # 指定操作哪个数据库
    charset='utf8'
)
# ... 在此处进行数据操作
# 需要使用 游标(对象)来操作数据库
cursor = conn.cursor()   # 创建一个游标
# 写一条SQL 语句交给 游标来执行
create_dep = '''CREATE TABLE departments(
id INT,
dep_name VARCHAR (20),
PRIMARY KEY(id)
)'''

# 使用游标 来执行 SQL 语句
cursor.execute(create_dep)  # 写入 SQL 语句
conn.commit()  # 提交SQL 语句到 服务器去执行

# 如果不再执行SQL 语句则 需要关闭游标
cursor.close()

# 操作完数据库,断开连接
conn.close()   # > quit;  退出连接
