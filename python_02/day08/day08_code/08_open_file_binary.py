

# 1. 打开文件
try:
    myfile = open('./hello.txt', 'rb')
    # 2. 读/写文件
    b = myfile.read()
    # 3. 关闭文件
    myfile.close()

    print('文件中的内容是：', b.decode())
except OSError:
    print('文件打开失败')