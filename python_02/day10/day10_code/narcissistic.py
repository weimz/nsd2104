# 算出 100 ~ 999 的水仙花数
# 水仙花数是 指百位的3次方 + 十位的3次方  + 各位的3次方  等于原数的数
# 打印出所有的 水仙花数

def print_narcissistic():
    # 方法3
    for bai in range(1, 10):
        for shi in range(10):
            for ge in range(10):
                x = bai * 100 + shi * 10 + ge
                if x == bai ** 3 + shi ** 3 + ge ** 3:
                    print(x)
    # 方法2
    # for x in range(100, 1000):
    #     s = str(x)  # 把数字转换为文字
    #     bai = int(s[0])   # 百位
    #     shi = int(s[1])  # 十位
    #     ge = int(s[2])  #  各位
    #     if x == bai ** 3 + shi ** 3 + ge ** 3:
    #         print(x)
    # 方法1
    # for x in range(100, 1000):
    #     bai = x // 100   # 百位
    #     shi = x // 10 % 10  # 十位
    #     ge = x % 10  #  各位
    #     if x == bai ** 3 + shi ** 3 + ge ** 3:
    #         print(x)

if __name__ == '__main__':
    print_narcissistic()