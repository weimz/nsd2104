# 北京的出租车计价器的规则
#     3公里以内 收费 13元 起步价
#     3公里以外, 每公里加收 2.3 元(又称单价)
#     超过 15 公里以后,  每公里再加收单价的 50% (1.15元) 的空驶费
# 要求: 输入公里数，打印出费用的金额
# 课间休息 10:11 回来
km = float(input('请输入公里数: '))
fee = 0
# 方法2
if km > 0:
    fee = fee + 13
# 计算多余3公里以外的钱数
if km > 3:
    fee = fee + (km - 3) * 2.3
# 计算多余15公里以外的钱数
if km > 15:
    fee = fee + (km - 15) * 1.15  # 2.3*.5


# 方法1
# if 0 < km <= 3:
#     fee = 13
# elif 3 < km <= 15:
#     fee = 13 + (km - 3) * 2.3
# elif km > 15:
#     fee = 13 + (km - 3) * 2.3 + (km - 15) * 2.3 * .5

print('金额', fee, '元')

