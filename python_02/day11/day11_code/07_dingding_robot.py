import requests
import json

# url = '您机器人的webhook地址'
url = 'https://oapi.dingtalk.com/robot/send?access_token=9d6b7923c031c0245ab9979a160656b8b656a74cd91ee24d7051c000ba405656'
headers = {'Content-Type': 'application/json; charset=UTF-8'}
data = {
    "msgtype": "text",
    "text": {
        "content": "好好学习天天向上我就是我, 是不一样的烟火@156xxxx8827,有内鬼终止交易！"
    },
    "at": {
        "atMobiles": [  # @哪些人
            # "156xxxx8827",
            # "189xxxx8325"
        ],
        "isAtAll": False  # @所有人
    }
}
data = {
    "msgtype": "markdown",
    "markdown": {
        "title": "春节放假通知",
        "text": """## 放假通知
![](http://aid.codeboy.com:8008/static/python/_images/202102.jpeg)
好好学习天天向上 [TMOOC](http://tmooc.cn)
"""
    },
    "at": {
        "atMobiles": [
            "13488820215"
        ],
        "isAtAll": False
    }
}

data = {
     "msgtype": "markdown",
     "markdown": {
         "title": "七夕节",
         "text": "#### 七夕节\n> 银烛秋光冷画屏，轻罗小扇扑流萤。天街夜色凉如水，卧看牵牛织女星。\n好好学习天天向上 \n> ![screenshot](https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png)\n> ###### 七夕 [七夕](https://baike.sogou.com/v179909070.htm) \n"
     },
      "at": {
          "atMobiles": [
          ],
          "isAtAll": False
      }
 }


r = requests.post(url, headers=headers, data=json.dumps(data))
print(r.json())