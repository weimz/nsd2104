# 写一个函数
# myrange
# 实现和
# range
# 一样的功能， 但返回列表
#
#
# def myrange(start, stop=None, step=None):
#     return range(start, stop, step)
#
#
# if stop == None:  # 这是一个参数
#     return list(range(0, start))
# ...
# 此处自己完成
#
# for x in myrange(4):
#     print(x)  # 0, 1, 2, 3

def myrange(start, stop=None, step=None):
    # 1. 确定开始, start, 结束 stop 和 步长 step 三个值
    if stop is None:  # 没有第二个参数
        stop = start
        start = 0
    if step is None:  # 确定第三个参数
        step = 1
    L = []
    # 2. 生成相应的数字，放入列表
    # 3. 考虑 step 是正数还是负数作相应的处理
    if step > 0:
        i = start
        while i < stop:
            L.append(i)  # 把生成的i 追加到列表中
            i += step
    elif step < 0:
        i = start
        while i > stop:
            L.append(i)
            i += step
    return L


for x in myrange(4):    # for x in [0, 1, 2, 3]
    print(x)  # 0, 1, 2, 3

print('=============')

for x in myrange(3, 6):   # for x in [3, 4, 5]
    print(x)  # 3, 4, 5

print('=============')

for x in myrange(1, 10, 2):   # for x in [1, 3, 5, 7, 9]
    print(x)  # 1, 3, 5, 7, 9

print('---------------')
for x in myrange(5, 0, -2):  # for x in [5, 3, 1]
    print(x)