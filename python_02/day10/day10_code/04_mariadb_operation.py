# 此示例示意 数据库的增删改查操


# file: pymysql_create_table.py

# 导入 pymysql 包
import pymysql

# 连接数据库

conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',
    db='nsd2104',   # 指定操作哪一个数据库
    charset='utf8'   # 指定操作的字符集
)

# 操作数据库
# 需要使用 游标来操作数据库
cursor = conn.cursor()  # 创建游标

# 在此处写SQL语句 进行增删改查操作
# 1. 插入数据
# insert_sql = 'INSERT INTO departments VALUES (%s, %s)'
# 1.1 插入一条数据到表 departments
# cursor.execute(insert_sql, (1, '人事部'))
# 1.2 插入多行数据, 用 executemany 方法 第二个参数是 列表
# cursor.executemany(insert_sql, [
#     (2, '运维部'),
#     (3, '开发部'),
#     (4, '测试部'),
#     (5, '财务部'),
# ])
# conn.commit()  # 把SQL 语句提交到服务器

# 2. 查询数据
select_sql = 'SELECT id, dep_name FROM departments'
cursor.execute(select_sql)
#2.1 取出一行数据
result1 = cursor.fetchone()
print(result1)
#2.2 取出2行数据
result2 = cursor.fetchmany(2)
print(result2)
# 2.3 取出剩余的全部数据
result3 = cursor.fetchall()
print(result3)
# 课间休息： 16：14 回来
# 3. 修改
# update_sql = 'UPDATE departments SET dep_name=%s WHERE dep_name=%s'
# cursor.execute(update_sql, ('人力资源部', '人事部'))
# conn.commit()  # 提交

# 4. 删除
# delete_sql = 'DELETE FROM departments WHERE id=%s'
# r = cursor.execute(delete_sql, (5,))
# conn.commit()

# 如果不再执行SQL 语句则 需要关闭游标
cursor.close()

# 操作完数据库，断开连接
conn.close()  # > quit;