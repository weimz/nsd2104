
def myfunc(a, b, *args, c, d, **kwargs):
    print(a, b, 'args=', args, c, d, 'kwargs=', kwargs)

myfunc(1, 2, 3, 4, c=11, d=22, e=33, f=44)
myfunc(1, 2, 3, 4, c=11, d=22)

