# 写一个猜数字游戏 让电脑随机生成一个 0 ~ 100 的整数让用来猜
# 如果 您输入的数大于电脑生产的数，提示：“您猜大了”， 继续猜
# 如果 您输入的数小于电脑生产的数，提示：“您猜小了”， 继续猜
# 当 您输入的数等于电脑生产的数，提示："恭喜您猜对了" 打印猜的次数后退出程序

import random

def guess_number():
    x = random.randint(0, 100)  # 电脑生成的数x
    count = 0 # 记录次数
    while True:
        y = int(input('请输入：'))
        count += 1
        if y > x:
            print('您输入大了')
        elif y < x:
            print('您输入小了')
        else:
            print('恭喜您猜对了！')
            break
    print('您共猜了', count, '次')
guess_number()