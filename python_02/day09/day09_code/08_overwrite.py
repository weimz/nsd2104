class Human:  # 定义一个人类
    def say(self, what):
        print("人说:", what)
  
class Student(Human):
    def say(self, what):
        print('student 说', what)

h1 = Human()
h1.say('天气真好')

s1 = Student()
s1.say('有点累')

