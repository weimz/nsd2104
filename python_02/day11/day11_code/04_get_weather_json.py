
# 使用 request 模块向 中国天气网的网站
# 发送GET 请求，来获取北京，上海等天气的信息
# 北京的URL地址：
# http://www.weather.com.cn/data/sk/101010100.html
# 上海的URL地址：
# http://www.weather.com.cn/data/sk/101020100.html

#1. 导入 requests 模块
import requests
import json

# 2. 发送GET请求
url = 'http://www.weather.com.cn/data/sk/101010100.html'
resp = requests.get(url)

# 3. 将 响应(response)的编码由默认的'ISO-8859-1'改为'utf8'
resp.encoding='utf8'
# 4. 使用响应(response)的 resp.text 获取字符串
s_json = resp.text
# 5. 将 json 转化为 Python 的数据类型
# print(s_json)
obj = json.loads(s_json)  # 课间休息： 15：13 回来
print(obj)
print('城市：', obj["weatherinfo"]['city'])
print('风速：', obj["weatherinfo"]['WS'])