# 1. 写一个程序，输入任意个正整数，当输入负数是结束输入,
# 将输入的正整数写入文件data.txt 中
# 如:
#   请输入: 1
#   请输入: 2
#   请输入: 3
#   请输入: 4
#   请输入: -1
# 文件中的内容是:
# 1
# 2
# 3
# 4

file = open('data.txt', 'w')
# 读取输入
while True:
    s = input('请输入')
    number = int(s)
    if number < 0:
         break
    file.write(str(number))
    file.write('\n')  # 写一个换行
file.close()







