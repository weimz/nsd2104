# 已知, 局域网的IP 都是: 192.168.xxx.yyy
# xxx 从 0 ~ 9, yyy 从 1 ~ 254
# 写程序，生成 从 192.168.0.1 ~ 192.168.10.254   区间的 2540 个 IP 地址
# # 提示用循环嵌套

ips = []

for xxx in range(0, 10):
    for yyy in range(1, 255):
        ip = '192.168.%d.%d' % (xxx, yyy)
        # ip = '192.168.' + str(xxx) + '.' + str(yyy)
        # print(ip)
        ips.append(ip)

print(ips)
