# 写一个程序， 生产6位由数字组成的随机密码
import random

def get_random_password():
    s = ''
    for _ in range(6):  # 循环六次
        s += str(random.randint(0, 9))
    return s

print(get_random_password())
print(get_random_password())