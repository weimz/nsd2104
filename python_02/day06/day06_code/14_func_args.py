

def myfunc3(a, b, *args):
    print(a, b, args)

myfunc3(11, 22)
myfunc3(1, 2, 3, 4, 5)  # 1-->a, 2-->b, (3,4,5)-->args

