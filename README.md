# Python 授课资料

### NSD Python 授课笔记阅读和下载方法

- 在线观看笔记课代码的方法

  > 登陆码云网站，点击如下链接进入笔记下载地址

  https://gitee.com/weimz/nsd2104

- 课上笔记如果本地没有任何资料，第一次下载：

```shell
git clone https://gitee.com/weimz/nsd2104.git
```

> 在 redhat 8 下如果不能运行 git 命令。需要用yum安装git命令
>
> 方法如下:
>
> ```
> yum install git
> ```

- 每日更新笔记：

```shell
cd nsd2104
git pull
```

### Python 开发环境 PyCharm 安装方法

Redhat 8 下安装方法详见如下文档: 

​    [pycharm_install/pycharm_install_4_redhat8.md](pycharm_install/pycharm_install_4_redhat8.md)

- 也可以仿制 如下 CentOS8 下的安装方法进行安装

  - cent8 环境下准备python编程环境PyCharm 的视频和文档

  链接: https://pan.baidu.com/s/1AjlCVUAnLn3FzKYe0dCkDg 

  提取码：gzd1

