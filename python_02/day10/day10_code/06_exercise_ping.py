# 写一个程序，测试此网络内,
# 192.168.1.1 ~  192.168.1.254 之间的机器，哪些开机，哪些关机
import subprocess
def ping(ip):
    cmd = 'ping -c2 %s &> /dev/null' % ip
    r = subprocess.run(cmd, shell=True)
    if r.returncode == 0:
        print(ip, '通')
    else:
        print(ip, '不通')

if __name__ == '__main__':
    for i in range(1, 255):
        ipv4 = '192.168.0.%d' % i
        ping(ipv4)