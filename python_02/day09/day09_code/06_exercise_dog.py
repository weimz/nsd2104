class Dog:
    def __init__(self, c, k):
        self.color = c  # 颜色
        self.kind = k  # 品种
        self.foods = []  # 保存食物

    def eat(self, weight, food):
        print(self.color, '的',self.kind,  '吃了',
              weight, '斤', food)
        self.foods.append(food)

    def info(self):
        print(self.color, '的', self.kind, '吃过',self.foods)
    
dog1 = Dog('白色', '藏獒')
dog1.eat(1, '羊肉')   # 白色 的 藏獒 吃了 1 斤 羊肉,
dog2 = Dog('灰色', '导盲犬')
dog2.eat(2, '狗粮')   # 灰色 的 导盲犬  吃了 2 斤 狗粮,

dog1.eat(2, '牛肉')   # 白色 的 藏獒 吃了 2 斤 牛肉,
dog1.info()   # 白色 的 藏獒 吃过 ['羊肉', '牛肉']
dog2.info()   # 灰色 的 导盲犬 吃过 ['狗粮']
