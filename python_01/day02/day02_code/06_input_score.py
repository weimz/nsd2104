# 写一个程序， 06_input_score.py, 输入一个学生的成绩，
# 如果成绩在 60~100 之间提示"及格", 否则提示不及格


score = int(input('请输入成绩: '))
# if 60 <= score <= 100:
if score >= 60 and score <= 100:
    print('及格')
else:
    print('不及格')