
import requests
import json

# API 地址
url = 'http://jisuqgtq.market.alicloudapi.com/weather/query'

# 请求头
headers = {
    # 根据API的要求，定义相对应的Content - Type
    'Content-Type': 'application/json; charset=UTF-8',
    # 权限指定 APPCODE
    'Authorization': 'APPCODE e3e8438c45584a768f11eef0a99f2b9a'
}

# 设置查询参数
params = {'citycode': '101010100'}  # 城市代码

r = requests.get(url, headers=headers, params=params)

s = r.text
print(s)
data = r.json()  # 等同于 json.loads(r.content.decode())

# 使用pprint 模块进行打印
import pprint
pprint.pprint(data)

