def myfunc4(a, b, *args, c, d):
    print(a, b, 'args=', args, c, d)

myfunc4(11, 22, 33, 44, d=200, c=100)
myfunc4(11, 22, c=33, d=44)
myfunc4(b=2, a=1, d=4, c=3)